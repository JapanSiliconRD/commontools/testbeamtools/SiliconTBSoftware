// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dOdIsrcdIOnlineMonWindow_Dict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "OnlineMonWindow/OnlineMonWindow.hh"
#include "OnlineMonWindow/OnlineMonWindow.hh"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void delete_OnlineMonWindow(void *p);
   static void deleteArray_OnlineMonWindow(void *p);
   static void destruct_OnlineMonWindow(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::OnlineMonWindow*)
   {
      ::OnlineMonWindow *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::OnlineMonWindow >(0);
      static ::ROOT::TGenericClassInfo 
         instance("OnlineMonWindow", ::OnlineMonWindow::Class_Version(), "OnlineMonWindow/OnlineMonWindow.hh", 36,
                  typeid(::OnlineMonWindow), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::OnlineMonWindow::Dictionary, isa_proxy, 4,
                  sizeof(::OnlineMonWindow) );
      instance.SetDelete(&delete_OnlineMonWindow);
      instance.SetDeleteArray(&deleteArray_OnlineMonWindow);
      instance.SetDestructor(&destruct_OnlineMonWindow);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::OnlineMonWindow*)
   {
      return GenerateInitInstanceLocal((::OnlineMonWindow*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::OnlineMonWindow*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr OnlineMonWindow::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *OnlineMonWindow::Class_Name()
{
   return "OnlineMonWindow";
}

//______________________________________________________________________________
const char *OnlineMonWindow::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::OnlineMonWindow*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int OnlineMonWindow::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::OnlineMonWindow*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *OnlineMonWindow::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::OnlineMonWindow*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *OnlineMonWindow::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::OnlineMonWindow*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void OnlineMonWindow::Streamer(TBuffer &R__b)
{
   // Stream an object of class OnlineMonWindow.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(OnlineMonWindow::Class(),this);
   } else {
      R__b.WriteClassBuffer(OnlineMonWindow::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_OnlineMonWindow(void *p) {
      delete ((::OnlineMonWindow*)p);
   }
   static void deleteArray_OnlineMonWindow(void *p) {
      delete [] ((::OnlineMonWindow*)p);
   }
   static void destruct_OnlineMonWindow(void *p) {
      typedef ::OnlineMonWindow current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::OnlineMonWindow

namespace ROOT {
   static TClass *vectorlEstringgR_Dictionary();
   static void vectorlEstringgR_TClassManip(TClass*);
   static void *new_vectorlEstringgR(void *p = 0);
   static void *newArray_vectorlEstringgR(Long_t size, void *p);
   static void delete_vectorlEstringgR(void *p);
   static void deleteArray_vectorlEstringgR(void *p);
   static void destruct_vectorlEstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<string>*)
   {
      vector<string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<string>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<string>", -2, "vector", 216,
                  typeid(vector<string>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEstringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<string>) );
      instance.SetNew(&new_vectorlEstringgR);
      instance.SetNewArray(&newArray_vectorlEstringgR);
      instance.SetDelete(&delete_vectorlEstringgR);
      instance.SetDeleteArray(&deleteArray_vectorlEstringgR);
      instance.SetDestructor(&destruct_vectorlEstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<string> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<string>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<string>*)0x0)->GetClass();
      vectorlEstringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEstringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string> : new vector<string>;
   }
   static void *newArray_vectorlEstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string>[nElements] : new vector<string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEstringgR(void *p) {
      delete ((vector<string>*)p);
   }
   static void deleteArray_vectorlEstringgR(void *p) {
      delete [] ((vector<string>*)p);
   }
   static void destruct_vectorlEstringgR(void *p) {
      typedef vector<string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<string>

namespace ROOT {
   static TClass *vectorlETGListTreeItemmUgR_Dictionary();
   static void vectorlETGListTreeItemmUgR_TClassManip(TClass*);
   static void *new_vectorlETGListTreeItemmUgR(void *p = 0);
   static void *newArray_vectorlETGListTreeItemmUgR(Long_t size, void *p);
   static void delete_vectorlETGListTreeItemmUgR(void *p);
   static void deleteArray_vectorlETGListTreeItemmUgR(void *p);
   static void destruct_vectorlETGListTreeItemmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<TGListTreeItem*>*)
   {
      vector<TGListTreeItem*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<TGListTreeItem*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<TGListTreeItem*>", -2, "vector", 216,
                  typeid(vector<TGListTreeItem*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlETGListTreeItemmUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<TGListTreeItem*>) );
      instance.SetNew(&new_vectorlETGListTreeItemmUgR);
      instance.SetNewArray(&newArray_vectorlETGListTreeItemmUgR);
      instance.SetDelete(&delete_vectorlETGListTreeItemmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlETGListTreeItemmUgR);
      instance.SetDestructor(&destruct_vectorlETGListTreeItemmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<TGListTreeItem*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<TGListTreeItem*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlETGListTreeItemmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<TGListTreeItem*>*)0x0)->GetClass();
      vectorlETGListTreeItemmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlETGListTreeItemmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlETGListTreeItemmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TGListTreeItem*> : new vector<TGListTreeItem*>;
   }
   static void *newArray_vectorlETGListTreeItemmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TGListTreeItem*>[nElements] : new vector<TGListTreeItem*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlETGListTreeItemmUgR(void *p) {
      delete ((vector<TGListTreeItem*>*)p);
   }
   static void deleteArray_vectorlETGListTreeItemmUgR(void *p) {
      delete [] ((vector<TGListTreeItem*>*)p);
   }
   static void destruct_vectorlETGListTreeItemmUgR(void *p) {
      typedef vector<TGListTreeItem*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<TGListTreeItem*>

namespace ROOT {
   static TClass *maplEstringcOvectorlEstringgRsPgR_Dictionary();
   static void maplEstringcOvectorlEstringgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlEstringgRsPgR(void *p = 0);
   static void *newArray_maplEstringcOvectorlEstringgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlEstringgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlEstringgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlEstringgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<string> >*)
   {
      map<string,vector<string> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<string> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<string> >", -2, "map", 99,
                  typeid(map<string,vector<string> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlEstringgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,vector<string> >) );
      instance.SetNew(&new_maplEstringcOvectorlEstringgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlEstringgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlEstringgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlEstringgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlEstringgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<string> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,vector<string> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlEstringgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,vector<string> >*)0x0)->GetClass();
      maplEstringcOvectorlEstringgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlEstringgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlEstringgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<string> > : new map<string,vector<string> >;
   }
   static void *newArray_maplEstringcOvectorlEstringgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<string> >[nElements] : new map<string,vector<string> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlEstringgRsPgR(void *p) {
      delete ((map<string,vector<string> >*)p);
   }
   static void deleteArray_maplEstringcOvectorlEstringgRsPgR(void *p) {
      delete [] ((map<string,vector<string> >*)p);
   }
   static void destruct_maplEstringcOvectorlEstringgRsPgR(void *p) {
      typedef map<string,vector<string> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<string> >

namespace ROOT {
   static TClass *maplEstringcOunsignedsPintgR_Dictionary();
   static void maplEstringcOunsignedsPintgR_TClassManip(TClass*);
   static void *new_maplEstringcOunsignedsPintgR(void *p = 0);
   static void *newArray_maplEstringcOunsignedsPintgR(Long_t size, void *p);
   static void delete_maplEstringcOunsignedsPintgR(void *p);
   static void deleteArray_maplEstringcOunsignedsPintgR(void *p);
   static void destruct_maplEstringcOunsignedsPintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,unsigned int>*)
   {
      map<string,unsigned int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,unsigned int>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,unsigned int>", -2, "map", 99,
                  typeid(map<string,unsigned int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOunsignedsPintgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,unsigned int>) );
      instance.SetNew(&new_maplEstringcOunsignedsPintgR);
      instance.SetNewArray(&newArray_maplEstringcOunsignedsPintgR);
      instance.SetDelete(&delete_maplEstringcOunsignedsPintgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOunsignedsPintgR);
      instance.SetDestructor(&destruct_maplEstringcOunsignedsPintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,unsigned int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,unsigned int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOunsignedsPintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,unsigned int>*)0x0)->GetClass();
      maplEstringcOunsignedsPintgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOunsignedsPintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOunsignedsPintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,unsigned int> : new map<string,unsigned int>;
   }
   static void *newArray_maplEstringcOunsignedsPintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,unsigned int>[nElements] : new map<string,unsigned int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOunsignedsPintgR(void *p) {
      delete ((map<string,unsigned int>*)p);
   }
   static void deleteArray_maplEstringcOunsignedsPintgR(void *p) {
      delete [] ((map<string,unsigned int>*)p);
   }
   static void destruct_maplEstringcOunsignedsPintgR(void *p) {
      typedef map<string,unsigned int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,unsigned int>

namespace ROOT {
   static TClass *maplEstringcOstringgR_Dictionary();
   static void maplEstringcOstringgR_TClassManip(TClass*);
   static void *new_maplEstringcOstringgR(void *p = 0);
   static void *newArray_maplEstringcOstringgR(Long_t size, void *p);
   static void delete_maplEstringcOstringgR(void *p);
   static void deleteArray_maplEstringcOstringgR(void *p);
   static void destruct_maplEstringcOstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,string>*)
   {
      map<string,string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,string>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,string>", -2, "map", 99,
                  typeid(map<string,string>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOstringgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,string>) );
      instance.SetNew(&new_maplEstringcOstringgR);
      instance.SetNewArray(&newArray_maplEstringcOstringgR);
      instance.SetDelete(&delete_maplEstringcOstringgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOstringgR);
      instance.SetDestructor(&destruct_maplEstringcOstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,string> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,string>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,string>*)0x0)->GetClass();
      maplEstringcOstringgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOstringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,string> : new map<string,string>;
   }
   static void *newArray_maplEstringcOstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,string>[nElements] : new map<string,string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOstringgR(void *p) {
      delete ((map<string,string>*)p);
   }
   static void deleteArray_maplEstringcOstringgR(void *p) {
      delete [] ((map<string,string>*)p);
   }
   static void destruct_maplEstringcOstringgR(void *p) {
      typedef map<string,string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,string>

namespace ROOT {
   static TClass *maplEstringcOTH1mUgR_Dictionary();
   static void maplEstringcOTH1mUgR_TClassManip(TClass*);
   static void *new_maplEstringcOTH1mUgR(void *p = 0);
   static void *newArray_maplEstringcOTH1mUgR(Long_t size, void *p);
   static void delete_maplEstringcOTH1mUgR(void *p);
   static void deleteArray_maplEstringcOTH1mUgR(void *p);
   static void destruct_maplEstringcOTH1mUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,TH1*>*)
   {
      map<string,TH1*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,TH1*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,TH1*>", -2, "map", 99,
                  typeid(map<string,TH1*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOTH1mUgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,TH1*>) );
      instance.SetNew(&new_maplEstringcOTH1mUgR);
      instance.SetNewArray(&newArray_maplEstringcOTH1mUgR);
      instance.SetDelete(&delete_maplEstringcOTH1mUgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOTH1mUgR);
      instance.SetDestructor(&destruct_maplEstringcOTH1mUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,TH1*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,TH1*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOTH1mUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,TH1*>*)0x0)->GetClass();
      maplEstringcOTH1mUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOTH1mUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOTH1mUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,TH1*> : new map<string,TH1*>;
   }
   static void *newArray_maplEstringcOTH1mUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,TH1*>[nElements] : new map<string,TH1*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOTH1mUgR(void *p) {
      delete ((map<string,TH1*>*)p);
   }
   static void deleteArray_maplEstringcOTH1mUgR(void *p) {
      delete [] ((map<string,TH1*>*)p);
   }
   static void destruct_maplEstringcOTH1mUgR(void *p) {
      typedef map<string,TH1*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,TH1*>

namespace ROOT {
   static TClass *maplEstringcOTGListTreeItemmUgR_Dictionary();
   static void maplEstringcOTGListTreeItemmUgR_TClassManip(TClass*);
   static void *new_maplEstringcOTGListTreeItemmUgR(void *p = 0);
   static void *newArray_maplEstringcOTGListTreeItemmUgR(Long_t size, void *p);
   static void delete_maplEstringcOTGListTreeItemmUgR(void *p);
   static void deleteArray_maplEstringcOTGListTreeItemmUgR(void *p);
   static void destruct_maplEstringcOTGListTreeItemmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,TGListTreeItem*>*)
   {
      map<string,TGListTreeItem*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,TGListTreeItem*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,TGListTreeItem*>", -2, "map", 99,
                  typeid(map<string,TGListTreeItem*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOTGListTreeItemmUgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,TGListTreeItem*>) );
      instance.SetNew(&new_maplEstringcOTGListTreeItemmUgR);
      instance.SetNewArray(&newArray_maplEstringcOTGListTreeItemmUgR);
      instance.SetDelete(&delete_maplEstringcOTGListTreeItemmUgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOTGListTreeItemmUgR);
      instance.SetDestructor(&destruct_maplEstringcOTGListTreeItemmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,TGListTreeItem*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,TGListTreeItem*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOTGListTreeItemmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,TGListTreeItem*>*)0x0)->GetClass();
      maplEstringcOTGListTreeItemmUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOTGListTreeItemmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOTGListTreeItemmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,TGListTreeItem*> : new map<string,TGListTreeItem*>;
   }
   static void *newArray_maplEstringcOTGListTreeItemmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,TGListTreeItem*>[nElements] : new map<string,TGListTreeItem*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOTGListTreeItemmUgR(void *p) {
      delete ((map<string,TGListTreeItem*>*)p);
   }
   static void deleteArray_maplEstringcOTGListTreeItemmUgR(void *p) {
      delete [] ((map<string,TGListTreeItem*>*)p);
   }
   static void destruct_maplEstringcOTGListTreeItemmUgR(void *p) {
      typedef map<string,TGListTreeItem*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,TGListTreeItem*>

namespace ROOT {
   static TClass *maplETGListTreeItemmUcOstringgR_Dictionary();
   static void maplETGListTreeItemmUcOstringgR_TClassManip(TClass*);
   static void *new_maplETGListTreeItemmUcOstringgR(void *p = 0);
   static void *newArray_maplETGListTreeItemmUcOstringgR(Long_t size, void *p);
   static void delete_maplETGListTreeItemmUcOstringgR(void *p);
   static void deleteArray_maplETGListTreeItemmUcOstringgR(void *p);
   static void destruct_maplETGListTreeItemmUcOstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TGListTreeItem*,string>*)
   {
      map<TGListTreeItem*,string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TGListTreeItem*,string>));
      static ::ROOT::TGenericClassInfo 
         instance("map<TGListTreeItem*,string>", -2, "map", 99,
                  typeid(map<TGListTreeItem*,string>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETGListTreeItemmUcOstringgR_Dictionary, isa_proxy, 0,
                  sizeof(map<TGListTreeItem*,string>) );
      instance.SetNew(&new_maplETGListTreeItemmUcOstringgR);
      instance.SetNewArray(&newArray_maplETGListTreeItemmUcOstringgR);
      instance.SetDelete(&delete_maplETGListTreeItemmUcOstringgR);
      instance.SetDeleteArray(&deleteArray_maplETGListTreeItemmUcOstringgR);
      instance.SetDestructor(&destruct_maplETGListTreeItemmUcOstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TGListTreeItem*,string> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<TGListTreeItem*,string>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETGListTreeItemmUcOstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TGListTreeItem*,string>*)0x0)->GetClass();
      maplETGListTreeItemmUcOstringgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETGListTreeItemmUcOstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETGListTreeItemmUcOstringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TGListTreeItem*,string> : new map<TGListTreeItem*,string>;
   }
   static void *newArray_maplETGListTreeItemmUcOstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TGListTreeItem*,string>[nElements] : new map<TGListTreeItem*,string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETGListTreeItemmUcOstringgR(void *p) {
      delete ((map<TGListTreeItem*,string>*)p);
   }
   static void deleteArray_maplETGListTreeItemmUcOstringgR(void *p) {
      delete [] ((map<TGListTreeItem*,string>*)p);
   }
   static void destruct_maplETGListTreeItemmUcOstringgR(void *p) {
      typedef map<TGListTreeItem*,string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TGListTreeItem*,string>

namespace {
  void TriggerDictionaryInitialization_OnlineMonWindow_Dict_Impl() {
    static const char* headers[] = {
"OnlineMonWindow/OnlineMonWindow.hh",
0
    };
    static const char* includePaths[] = {
"./src",
"OnlineMonWindow",
"Root",
"/home/atlasj/work/FNALtestbeam2019/SiliconTBSoftware-20190224//include",
"/usr/include/root",
"/home/atlasj/work/FNALtestbeam2019/SiliconTBSoftware-20190224/ExternalPackages/OnlineMonWindow/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "OnlineMonWindow_Dict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$OnlineMonWindow/OnlineMonWindow.hh")))  OnlineMonWindow;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "OnlineMonWindow_Dict dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "OnlineMonWindow/OnlineMonWindow.hh"
#ifndef __ONLINEMONITOR_LINKDEF__
#define __ONLINEMONITOR_LINKDEF__

#include "OnlineMonWindow/OnlineMonWindow.hh"
#ifdef __CINT__
#pragma link C++ class OnlineMonWindow+;
#endif
#endif

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"OnlineMonWindow", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("OnlineMonWindow_Dict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_OnlineMonWindow_Dict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_OnlineMonWindow_Dict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_OnlineMonWindow_Dict() {
  TriggerDictionaryInitialization_OnlineMonWindow_Dict_Impl();
}
