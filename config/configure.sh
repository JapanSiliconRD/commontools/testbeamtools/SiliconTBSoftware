#! /bin/bash

CONFIGDIR=`dirname $0`
CONFIGDIR=`readlink -f ${PWD}/$CONFIGDIR`
SISOFTDIR=${CONFIGDIR%config}
ROOTEXE=`which root ` 
GCCVERSION=`gcc --version | grep ^gcc | awk '{print $3}' | awk -F. '{print $1"."$2}'`
if [ `echo "$GCCVERSION >= 4.8" | bc -l` != 1 ]
then
    echo "setup g++ >=4.8 "
fi

if [ ! $ROOTEXE ]
then
    echo "setup ROOT before configure"
    exit
fi

for ii in 0
do
    echo "#!/bin/bash" 
    echo "export LANG=C"
    echo "export LC_ALL=C"

    GPPEXE=`which g++`
    if [ `echo $GPPEXE| awk -F/ '{print "/"$2"/"$3"/"$4}'` == "/cvmfs/sft.cern.ch/lcg" ]
    then
	echo source $CONFIGDIR/setupATLAS.sh
    else
	if [ ${ROOTEXE} = "/bin/root"  ]
	then
	    echo -n ""
	else
	    echo source ${ROOTEXE%root}thisroot.sh
	fi
	
	if [ `echo $GPPEXE| awk -F/ '{print "/"$2"/"$3"/"$4}'` == "/afs/cern.ch/sw" ]
	then
	    echo source /afs/cern.ch/sw/lcg/contrib/gcc/4.8/x86_64-slc6/setup.sh
	elif [ `echo $GPPEXE| awk -F/ '{print "/"$2"/"$3"/"$4}'` == "/opt/rh/devtoolset-7" ]
	then
	    echo source /opt/rh/devtoolset-7/enable
	fi
    fi
    echo "export SISOFTDIR=$SISOFTDIR"
    echo 'export LD_LIBRARY_PATH=${SISOFTDIR}/shlib:${SISOFTDIR}/OnlineMonitor/shlib:${SISOFTDIR}/OfflineAnalysis/shlib:${LD_LIBRARY_PATH}'
    
    #echo 'cd ${SOFTDIR}/OnlineMonitor/src'
    echo 'cd ${SISOFTDIR}/'
    
done > $CONFIGDIR/setup_local.sh
mkdir -p ${SISOFTDIR}/{include,shlib}
mkdir -p ${SISOFTDIR}/OnlineMonitor/{shlib,bin}
mkdir -p ${SISOFTDIR}/OfflineAnalysis/{shlib,bin}

cat  > $SISOFTDIR/CorePackages/Common/Common/config.h <<EOF
#ifndef __CONFIG__H__
#define __CONFIG__H__
#define PACKAGE_NAME "SiliconTBSoftware"
#define PACKAGE_VERSION "v0"
#define PACKAGE_STRING PACKAGE_NAME " " PACKAGE_VERSION
#define PACKAGE_SOURCE_DIR "$SISOFTDIR"
#endif 
EOF

