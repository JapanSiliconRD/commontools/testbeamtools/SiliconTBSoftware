#! /bin/bash

if [ $1 ] 
    then 
    VERSION=$1
else
    VERSION=6.04.06
fi
echo "Setting up root v$VERSION ... "
rootsystmp=/afs/cern.ch/sw/lcg/app/releases/ROOT/${VERSION}/x86_64-slc6-gcc48-opt/root
latestroot=`/bin/ls /afs/cern.ch/sw/lcg/app/releases/ROOT/6*/x86_64-slc6-gcc48-opt/root/bin/thisroot.sh | tail -1`
if [ $latestroot != "$rootsystmp/bin/thisroot.sh" ] 
then
    echo "new version may be available :"
    echo $latestroot
    
fi

source /afs/cern.ch/sw/lcg/contrib/gcc/4.8/x86_64-slc6/setup.sh
source $rootsystmp/bin/thisroot.sh

export PATH=/afs/cern.ch/sw/lcg/external/Python/2.7.3/i686-slc6-gcc47-opt/bin:$PATH
export LD_LIBRARY_PATH=/afs/cern.ch/sw/lcg/external/Python/2.7.3/i686-slc6-gcc47-opt/lib:$LD_LIBRARY_PATH


