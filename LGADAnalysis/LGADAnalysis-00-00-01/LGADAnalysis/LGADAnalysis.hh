#ifndef __LGADAnalysis_hh__
#define  __LGADAnalysis_hh__

#include <iomanip>
//#include "Common/header.hh"
#include "UserInterFace/UserInterFace.hh"
#include "HistogramTools/HistogramVariableStrage.hh"
#include "LGADNtupleInfo/LGADNtupleInfo.hh"


class LGADAnalysis : public LGADNtupleInfo{
public:
  void SetUserInterFace(UserInterFace _ui){ui=_ui;}
  LGADAnalysis(){};
  ~LGADAnalysis(){};
  void Execute(){
    Loop();
    std::cout << "Executed" << std::endl;
  }
  void SetChain(TTree *_chain){fChain=(TChain*)_chain;}

private:
  TChain *fChain;
  UserInterFace ui;
  TFile *m_OutputFile;
  HistogramVariableStrage *hvs;
  void Initialize();
  void Finalize();
  void Action();
  void initAction();
  void finAction();
  void Loop();
  void SetHistogram();
  void doNOTHING();
  void doPlotPulseHeight();
  int totalevent;
  int tmp_counter1;
  int tmp_counter2;
  int tmp_counter3;
  int evtno;


};


#endif
