#include "LGADAnalysis.hh"

void LGADAnalysis::Initialize(){

  totalevent=fChain->GetEntries();
  // Output File setting
  m_OutputFile = new  TFile(ui.getOutputFilename().c_str(),"recreate");
  std::cout << " setting histos ....  ";
  SetHistogram();
  std::cout << " done  " << std::endl;

  tmp_counter1=0;
  tmp_counter2=0;
  tmp_counter3=0;
  evtno=0;

}

void LGADAnalysis::initAction(){

}
void LGADAnalysis::finAction(){
}
void LGADAnalysis::Finalize(){


  m_OutputFile->Write();
  m_OutputFile->Close();
  std::cout << "tempolary counter 1 : " << tmp_counter1 << std::endl;;
  std::cout << "tempolary counter 2 : " << tmp_counter2 << std::endl;;
  std::cout << "tempolary counter 3 : " << tmp_counter3 << std::endl;;
  std::cout << "1/1+2 : " << ((double)tmp_counter1)/(tmp_counter1+tmp_counter2) << std::endl;;

  return;
}

void LGADAnalysis::Action(){
  if(ui.getJobType()=="") std::cerr << "no JobType was set...." << std::endl;
  else if(ui.getJobType()=="NONE") doNOTHING();
  else if(ui.getJobType()=="PLPH") doPlotPulseHeight();
  else std::cerr << "unknown jobtype :" << ui.getJobType() << std::endl;
}
void LGADAnalysis::Loop(){
  Initialize();
  Int_t nentries = Int_t(fChain->GetEntries());
  std::cerr << "Total Number of Events = "  << nentries << std::endl;
  int nLoopEvents; 
  if(ui.getEndEvent()==0)nLoopEvents=nentries;
  else nLoopEvents=ui.getEndEvent();
  std::cerr << nLoopEvents << std::endl;
  //  std::cerr << nentries << std::endl;
  for (Int_t jentry=0; jentry<nLoopEvents;jentry++) {
    if(jentry<ui.getStartEvent())continue;
    //    std::cout << "c1"<< std::endl;
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0){
      std::cout << "ientry " << ientry << " observed. Ending job..." << std::endl;
      break;
    }
    //    ClearBranch();
    fChain->GetEntry(jentry); 
    //    if(jentry%1000==0){
    if(1){
      std::cout << std::setw(7) << jentry << "th event process" << std::endl;
//      std::cout << std::setw(7) << jentry << "th event process"
//                << " >> Run " << runNumber << " Event " << eventNumber << " " ;
    }
    initAction();
    Action();
    finAction();
    evtno++;
  }
  Finalize();
  return;
}

void LGADAnalysis::doNOTHING(){
  //  std::cout << "nothing to do " << std::endl;
//  if(evtno==200){
//    for(int ii=0;ii<1024;ii++){
//      std::cout << ii << " " << channel[0][ii] << " " << channel[1][ii] << std::endl;
//    }
//  }
}
void LGADAnalysis::doPlotPulseHeight(){
  std::cout << "plot pulse height" << std::endl;
}


void LGADAnalysis::SetHistogram(){
}
