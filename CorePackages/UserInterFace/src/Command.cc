#include "Command.hh"

std::vector<std::string> Command::getCmdInfo(const std::string &str){
  m_infoList.clear();
  if (str.size() == 0) {
    std::cout << "command not found: " << str << std::endl;
    return m_infoList;
  }
  int ii=0;  while(str.at(ii)==' ') ii++;
  int cmdi=ii;  int cmdf=str.find(' ',ii)-ii;
  int jj=str.find(' ',ii)-ii;  while(str.at(jj)==' ') jj++;
  int valuei=jj;  int valuef=str.length();
  std::string cmd=str.substr(cmdi,cmdf);
  std::string value=str.substr(valuei,valuef);
  //  std::cout << "cmd   : '" << cmd  << "'" << std::endl;
  //  std::cout << "value : '" << value << "'" << std::endl;
  std::vector<std::string> s;
  s.push_back(cmd);
  s.push_back(value);
  return s;
}

