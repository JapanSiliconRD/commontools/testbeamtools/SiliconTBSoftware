#include "HistogramTools/HistogramVariableStrage.hh"


void HistogramVariableStrage::PrintHistName(){
  std::cout << "1D histgram :" << std::endl;
  for(unsigned int ii=0;ii<histname.size();ii++){
    std::cout << " " << histname[ii] << std::endl;
  }
  std::cout << "2D histgram :" << std::endl;
  for(unsigned int ii=0;ii<hist2Dname.size();ii++){
    std::cout << " " << hist2Dname[ii] << std::endl;
  }
  std::cout << "3D histgram :" << std::endl;
  for(unsigned int ii=0;ii<hist3Dname.size();ii++){
    std::cout << " " << hist3Dname[ii] << std::endl;
  }
}
void HistogramVariableStrage::Add4vecHistogram(std::string name,int nbins,double xmin,double xmax,int mnbins,double mxmin,double mxmax){
  std::vector<std::string>::iterator itr = std::find(histname.begin( ), histname.end( ), name+"_Pt" );
  if(itr != histname.end()){
    std::cerr << "histogram : " << name << " does exist..." << std::endl;
  }else{
    std::string fourvect[4]={"Pt","Eta","Phi","M"};
    double ar_xmin[3]={-5,-3.141592,mxmin};
    double ar_xmax[3]={5,3.141592,mxmax};
    for(int ii=0;ii<4;ii++){
      histname.push_back(name+"_"+fourvect[ii]);
      double tmpnbins=100;
      if(ii==0)tmpnbins=nbins;
      if(ii==3)tmpnbins=mnbins;
      TH1D* h1 = new TH1D((name+"_"+fourvect[ii]+"_"+suffix).c_str(),(name+"_"+fourvect[ii]+"_"+suffix).c_str(),tmpnbins,ii==0?xmin:ar_xmin[ii-1],ii==0?xmax:ar_xmax[ii-1]);
      h1->Sumw2();
      histstrage.push_back(h1);
    }
  }
}

void HistogramVariableStrage::AddHistogram(std::string name,int nbins,double xmin,double xmax,double ymin,double ymax){
  std::vector<std::string>::iterator itr = std::find(histname.begin( ), histname.end( ), name );
  if(itr != histname.end()){
    std::cerr << "histogram : " << name << " does exist..." << std::endl;
  }else{
    histname.push_back(name);
    TH1D* h1 = new TH1D((name+"_"+suffix).c_str(),(name+"_"+suffix).c_str(),nbins,xmin,xmax);
    h1->Sumw2();
    h1->SetMinimum(ymin);
    if(ymin<ymax){
      h1->SetMaximum(ymax);
    }
    histstrage.push_back(h1);
  }
}
void HistogramVariableStrage::Add2DHistogram(std::string name,int nxbins,double xmin,double xmax,int nybins,double ymin,double ymax){
  std::vector<std::string>::iterator itr = std::find(hist2Dname.begin( ), hist2Dname.end( ), name );
  if(itr != hist2Dname.end()){
    std::cerr << "histogram : " << name << " does exist..." << std::endl;
  }else{
    hist2Dname.push_back(name);
    TH2D* h1 = new TH2D((name+"_"+suffix).c_str(),(name+"_"+suffix).c_str(),nxbins,xmin,xmax,nybins,ymin,ymax);
    h1->Sumw2();
    hist2Dstrage.push_back(h1);
  }
}


void HistogramVariableStrage::Add3DHistogram(std::string name,int nxbins,double xmin,double xmax,int nybins,double ymin,double ymax,int nzbins,double zmin,double zmax){
  std::vector<std::string>::iterator itr = std::find(hist3Dname.begin( ), hist3Dname.end( ), name );
  if(itr != hist3Dname.end()){
    std::cerr << "histogram : " << name << " does exist..." << std::endl;
  }else{
    hist3Dname.push_back(name);
    TH3D* h1 = new TH3D((name+"_"+suffix).c_str(),(name+"_"+suffix).c_str(),nxbins,xmin,xmax,nybins,ymin,ymax,nzbins,zmin,zmax);
    h1->Sumw2();
    hist3Dstrage.push_back(h1);
  }
}

TH1D* HistogramVariableStrage::GetHistogram(std::string name){
  std::vector<std::string>::iterator itr = std::find(histname.begin(), histname.end(), name );
  if(itr != histname.end()){
    int num=-1;
    for(unsigned int ii=0;ii<histname.size();ii++){
      if(histname[ii]==name){
	num=ii;
	return histstrage[ii];
      }
    }
  }else{
    std::cerr << "histogram : " << name << " does not exist..." << std::endl;
  }    
  return 0;
}
TH2D* HistogramVariableStrage::Get2DHistogram(std::string name){
  std::vector<std::string>::iterator itr = std::find(hist2Dname.begin(), hist2Dname.end(), name );
  if(itr != hist2Dname.end()){
    int num=-1;
    for(unsigned int ii=0;ii<hist2Dname.size();ii++){
      if(hist2Dname[ii]==name){
	num=ii;
	return hist2Dstrage[ii];
      }
    }
  }else{
    std::cerr << "histogram : " << name << " does not exist..." << std::endl;
  }    
  return 0;
}


TH3D* HistogramVariableStrage::Get3DHistogram(std::string name){
  std::vector<std::string>::iterator itr = std::find(hist3Dname.begin(), hist3Dname.end(), name );
  if(itr != hist3Dname.end()){
    int num=-1;
    for(unsigned int ii=0;ii<hist3Dname.size();ii++){
      if(hist3Dname[ii]==name){
	num=ii;
	return hist3Dstrage[ii];
      }
    }
  }else{
    std::cerr << "histogram : " << name << " does not exist..." << std::endl;
  }    
  return 0;
}

std::vector<TH1D*> HistogramVariableStrage::Get4vecHistogram(std::string name){
  std::vector<std::string>::iterator itr = std::find(histname.begin(), histname.end(), name+"_Pt" );
  std::vector<TH1D*> hist4vec; 
  if(itr != histname.end()){
    int num=-1;
    for(unsigned int ii=0;ii<histname.size();ii++){
      if(histname[ii]==name+"_Pt"){
	hist4vec.clear();
	num=ii;
	hist4vec.push_back(GetHistogram(name+"_Pt"));
	hist4vec.push_back(GetHistogram(name+"_Eta"));
	hist4vec.push_back(GetHistogram(name+"_Phi"));
	hist4vec.push_back(GetHistogram(name+"_M"));
	return hist4vec;
      }
    }
  }else{
    std::cerr << "histogram : " << name+"_Pt" << " does not exist..." << std::endl;
  }    
  return hist4vec;
}

