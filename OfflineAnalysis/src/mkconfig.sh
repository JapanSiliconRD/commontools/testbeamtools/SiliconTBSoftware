#! /bin/bash
#DATABASEDIR=${SISOFTDIR%SiliconTBSoftware-*}
DATABASEDIR=/home/kojin/work/ITKTB/PPSdata/FNALtestbeam2019
SEABASTLUDIR=${DATABASEDIR}/SeabasTLU201902/SoftwareSeabasTLU-FNAL/SoftwareSeabasTLU-trunk/
HSIO2DIR="${DATABASEDIR}/HSIO2/20190220/Integration/"
XpressK7DIR="${DATABASEDIR}/XpressK7/Yarr/src/data"
DRS4DIR=${DATABASEDIR}/DRS4
if [ ! $1 ]
then
    SeabasTLUFILE=`ls  $SEABASTLUDIR/data/rawData_tlu_run*.bin | tail -1`
    SeabasTLURUN=`basename ${SeabasTLUFILE}`
    SeabasTLURUN=${SeabasTLURUN#rawData_tlu_run}
    SeabasTLURUN=${SeabasTLURUN%.bin}
    SeabasTLURUN=`echo "${SeabasTLURUN} +0 "| bc -l`
#while [ $num -ne 0 ]; do RUNNUM="0$RUNNUM" ; num=`echo "$num-1" | bc -l`;done
    echo "TLU runnumber = $SeabasTLURUN" 
    XpressK7FILE=`ls -d ${XpressK7DIR}/*_fnal_exttrigger/data.raw | tail -1`    XpressK7RUN=`echo ${XpressK7FILE%_fnal_exttrigger/data.raw}`
    XpressK7RUN=`basename ${XpressK7RUN}`
    XpressK7RUN=`echo $XpressK7RUN | bc -l `
    echo -n "runnumber for XpressK7 system  : "
    echo $XpressK7RUN

#    echo -n "runnumber for SVX4 system  : "
#    SVX4FILE=`ls ${DATABASEDIR}/SVX4/data/run_*.dat | tail -1`
#    SVX4RUN=`basename $SVX4FILE`
#    SVX4RUN=${SVX4RUN%.dat}
#    SVX4RUN=${SVX4RUN#run_}
#    SVX4RUN=`echo $SVX4RUN | bc -l`
#    echo $SVX4RUN

    echo -n "runnumber for HSIO2 system : "
    HSIO2RUN=`ls -d ${HSIO2DIR}/data/cosmic_* | tail -1`
    HSIO2FILE=`ls $HSIO2RUN/cosmic_*.dat`
    HSIO2RUN=`basename $HSIO2RUN`
    HSIO2RUN=${HSIO2RUN#cosmic_}
    HSIO2RUN=`echo $HSIO2RUN | bc -l`
    echo $HSIO2RUN

    echo -n "runnumber for DRS4 system : "
    DRS4FILE=`ls -trd ${DRS4DIR}/data/testrun-* | tail -1`
    DRS4RUN=`basename ${DRS4FILE%.dat}`
    DRS4RUN=${DRS4RUN#testrun-}
    DRS4RUN=`echo $DRS4RUN | bc -l`
    echo $DRS4RUN
    echo ""
    
    
    
    
else
    
    echo "run number " $1
    SeabasTLULOGFILE=`ls  $SEABASTLUDIR/log/rawData_tlu_run*0$1.log | tail -1`
    SeabasTLUFILE=`ls  $SEABASTLUDIR/data/rawData_tlu_run*$1.bin | tail -1`
    `cat $SeabasTLULOGFILE | head -4 | tail -3 | awk '{print "export "$3"="$6}'`

    echo -n "runnumber for XpressK7 system  : "
    echo $XpressK7 
    XpressK7FILE=`ls -d ${XpressK7DIR}/*${XpressK7}*_fnal_exttrigger/data.raw | tail -1`

    echo -n "runnumber for HSIO2 system  : "
    echo $HSIO2
    HSIO2FILE=`ls ${HSIO2DIR}/data/cosmic*${HSIO2}*/cosmic_*${HSIO2}*.dat| tail -1`

    echo -n "runnumber for DRS4 system  : "
    echo $DRS4
    DRS4FILE=`ls -trd ${DRS4DIR}/data/testrun-*$DRS4* | tail -1`


fi

echo "SeabasTLU "  $SeabasTLUFILE > config.txt
echo "HSIO2  "  $HSIO2FILE >> config.txt
echo "XpressK7  "  $XpressK7FILE >> config.txt
echo "DRS4  "  $DRS4FILE >> config.txt
#echo "SVX4  "  $SVX4FILE >> config.txt
