#ifndef __OFFLINEANALYSIS_HH__
#define __OFFLINEANALYSIS_HH__

#include <iostream>
#include <vector>
#include <thread>
#include "EventLoopManager/EventLoopManager.hh"

class OfflineAnalysis : public EventLoopManager{
private:
  std::string histfilename;
public:
  void setHistFileName(std::string hfn){histfilename=hfn;};
  OfflineAnalysis(){}
  ~OfflineAnalysis();
  void Finalize();
  void CheckPrealign();
};

#endif
