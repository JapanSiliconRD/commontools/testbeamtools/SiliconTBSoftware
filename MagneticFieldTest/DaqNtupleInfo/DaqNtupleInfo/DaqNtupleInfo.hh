//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Dec 17 03:42:02 2015 by ROOT version 5.34/25
// from TTree cosmic/cosmic data
// found on file: /home/kojin/work/Silicon/MagnetTest/TriggerLogic/Seabas2/SoftwareSeabasTLU/SoftwareSeabasTLU-trunk/data/Cryo_20151204_v1.root
//////////////////////////////////////////////////////////

#ifndef DaqNtupleInfo_h
#define DaqNtupleInfo_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class DaqNtupleInfo {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   UInt_t          DH;
   UInt_t          TID0;
   UInt_t          TID1;
   UInt_t          TDC0;
   UInt_t          TDC1;
   UInt_t          TDC2;
   UInt_t          TrigType;
   UInt_t          TrigID;
   UInt_t          TdcCount;

   // List of branches
   TBranch        *b_DH;   //!
   TBranch        *b_TID0;   //!
   TBranch        *b_TID1;   //!
   TBranch        *b_TDC0;   //!
   TBranch        *b_TDC1;   //!
   TBranch        *b_TDC2;   //!
   TBranch        *b_TrigType;   //!
   TBranch        *b_TrigID;   //!
   TBranch        *b_TdcCount;   //!

   DaqNtupleInfo(TTree *tree=0);
   virtual ~DaqNtupleInfo();
   virtual void     Action(){}
   virtual void     Finalize(){}
   virtual void     Initialize(){}
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef DaqNtupleInfo_cxx
DaqNtupleInfo::DaqNtupleInfo(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/home/kojin/work/Silicon/MagnetTest/TriggerLogic/Seabas2/SoftwareSeabasTLU/SoftwareSeabasTLU-trunk/data/Cryo_20151204_v1.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/home/kojin/work/Silicon/MagnetTest/TriggerLogic/Seabas2/SoftwareSeabasTLU/SoftwareSeabasTLU-trunk/data/Cryo_20151204_v1.root");
      }
      f->GetObject("cosmic",tree);

   }
   Init(tree);
}

DaqNtupleInfo::~DaqNtupleInfo()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t DaqNtupleInfo::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t DaqNtupleInfo::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void DaqNtupleInfo::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("DH", &DH, &b_DH);
   fChain->SetBranchAddress("TID0", &TID0, &b_TID0);
   fChain->SetBranchAddress("TID1", &TID1, &b_TID1);
   fChain->SetBranchAddress("TDC0", &TDC0, &b_TDC0);
   fChain->SetBranchAddress("TDC1", &TDC1, &b_TDC1);
   fChain->SetBranchAddress("TDC2", &TDC2, &b_TDC2);
   fChain->SetBranchAddress("TrigType", &TrigType, &b_TrigType);
   fChain->SetBranchAddress("TrigID", &TrigID, &b_TrigID);
   fChain->SetBranchAddress("TdcCount", &TdcCount, &b_TdcCount);
   Notify();
}

Bool_t DaqNtupleInfo::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void DaqNtupleInfo::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t DaqNtupleInfo::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef DaqNtupleInfo_cxx
