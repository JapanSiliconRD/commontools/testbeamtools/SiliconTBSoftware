#ifndef __ReadUSBpixData_HH_
#define __ReadUSBpixData_HH_
#include <iostream>
#include "TFile.h"
#include <string>
#include <fstream>
#include <vector>
#include "ReadUSBpixData/PixelEvents.hh"
class ReadUSBpixData {
private :
  PixelEvent events;
public : 
  ReadUSBpixData();
  ~ReadUSBpixData(){}
  int ReadFile(std::string file,int ly);
  PixelEvent GetEvent(){return events;}


};

#endif
