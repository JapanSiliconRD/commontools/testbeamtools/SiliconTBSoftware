#include "Hit/PixelHit.hh"
#include "ReadUSBpixData/PixelEvents.hh"
#include "Cluster/Cluster.hh"

void AnPixelEvent::MakeCluster(){
  for(unsigned int ihit=0;ihit<hits.size();ihit++){
    Cluster tmpclu;
    tmpclu.AddHitsForCluster(hits[ihit]);
    tmpclu.SetLayer(hits[ihit].GetLayer());
    int nhit=0;
    if(ihit==hits.size()-1) {
      tmpclu.GetClusterPosition(0);
      clusters.push_back(tmpclu);
      break;    
    }
    while(hits[ihit].isNext(hits[ihit+1])){
      tmpclu.AddHitsForCluster(hits[ihit+1]);
      ihit++; nhit++;
      if(ihit==hits.size()-1){
	break;
      }
    }
    bool debug=false;
    if(debug){
      std::cout << "cluster size  = " << tmpclu.GetHitsForCluster().size() << std::endl;
      if(tmpclu.GetHitsForCluster().size()>1){
	for(unsigned int ihit = 0 ; ihit<tmpclu.GetHitsForCluster().size(); ihit++){
	  std::cout << tmpclu.GetHitsForCluster()[ihit].Col() << " " << tmpclu.GetHitsForCluster()[ihit].Row() << std::endl;
	}
      }
    }
    tmpclu.GetClusterPosition(0);
    clusters.push_back(tmpclu);
    
  }

}
