#include "ReadUSBpixData.hh"
#include "PixelEvents.hh"

ReadUSBpixData::ReadUSBpixData(){
}

int ReadUSBpixData::ReadFile(std::string file,int ly){
  double ZPos[3]={0,16,42.4};
  std::ifstream ifs(file.c_str());
  std::string str;

  std::string type[3]={"B","C","A"};
  if(ifs.fail()) {
    std::cerr << "File do not exist." << std::endl;
    std::cout << file << std::endl;
    return -1;
  }

  char gomi1[]=""; 
  int gomi2=0;  
  int bcid=0;  
  AnPixelEvent *event = new AnPixelEvent();
  //  AnPixelEvent event;
  int eventnumber=0;
  int LV1=0;
  bool isfirstevent=true;
  bool islastevent=false;
  int ich=-1;
  int nch=-1;
  bool isfirstDH=false;
  bool debug=false;
  while(getline(ifs, str)) {
    
    if(str.find("CHANNEL",0)!= std::string::npos){
      nch++;
      islastevent=true;
      int iich=-1;
      sscanf(str.c_str(),"%s %d", gomi1, &iich);
      if(debug)std::cout << str << std::endl;
      //      if(nch>3)break;
      ich=iich;
      std::cout << "reading channel " << ich << std::endl;
    }else if(str.find("TD",0)!= std::string::npos){
      std::string tdstr=str;
      if(!isfirstevent){
	//	event.PrintPixelEvent();
	//	if(nch%4==0||(nch%4==1&&islastevent)){
	if(nch%4==0){
	  if(debug)std::cout << "adding event" << std::endl;
	  if(events.Add(event)==-1)break;
	}
      }
      isfirstevent=false;
      islastevent=false;

      LV1=0;
      //      std::cout << tdstr << std::endl;
      sscanf(tdstr.c_str(),"%s %d %d %d", gomi1, &gomi2, &gomi2, &eventnumber);
      //      if(eventnumber==28940)debug=true;
      //      else debug=false;
      if(debug)std::cout << str << std::endl;
      if(eventnumber%1000==0) std::cout << "reading event # "<< eventnumber << " ("<< ly <<"-" << nch%4 << ") .... " << std::endl;
      //      if(eventnumber>350000&&eventnumber%10==0&&nch%4==1) std::cout << "reading event # "<< eventnumber << " ("<< ly <<"-" << nch%4 << ") .... " << std::endl;
      if(nch%4==0){
	event = new AnPixelEvent();
	event->SetEventNumber(eventnumber);
      }
      //      std::cout << "event" << eventnumber << std::endl;
      isfirstDH=true;
    }else if(str.find("DH",0)!= std::string::npos){
      //      std::cout << str << std::endl;
      sscanf(str.c_str(),"%s %d %d %d", gomi1, &gomi2, &gomi2, &bcid);
      if(debug)std::cout << str << std::endl;
      LV1++;
      if(isfirstDH){
	event->SetBCID(bcid);
	isfirstDH=false;
      }
    }else if(str.find("DR",0)!= std::string::npos){
      int col,row,ntot,ntot2;
      //      std::cout << str << std::endl;
      sscanf(str.c_str(),"%s %d %d %d %d", gomi1, &col, &row, &ntot, &ntot2);
      if(debug)std::cout << str << std::endl;
      if(nch%4==0)event->AddHit(nch%4,col,row,ntot,ntot2,LV1,ZPos[ly],type[ly]);
      else {
	//	std::cout << "events.GetEvent(" << eventnumber << ").AddHit(" << nch << "," << col << "," << row << "," << ntot << ", " << ntot2 << ", " <<LV1 << " )" << std::endl;;
	//	std::cout << events.GetEvent(eventnumber)->GetHits().size() << std::endl;
	if(debug){
	  std::cout << "adding hit" << std::endl;
	  std::cout << events.Get()[events.Get().size()-1]->GetEventNumber() << std::endl;
	}
	events.GetThisEvent(eventnumber)->AddHit(nch%4,col,row,ntot,ntot2,LV1,ZPos[ly],type[ly]);
	//	std::cout << events.GetEvent(eventnumber)->GetHits().size() << std::endl;
      }
    }
  }
  //  std::cout << "c1" << std::endl;
  events.doClustering();
  //  std::cout << "c2" << std::endl;
  return 0;
}
