#ifndef _FILEREAD_HH
#define _FILEREAD_HH

#include <iostream>
#include "UserInterFace/UserInterFace.hh"
#include "TTree.h"
#include "TChain.h"
//#include "Common/header.hh"

class FileRead {
private:
  char buff[1024];

public:
  FileRead(){}
  ~FileRead(){}
  //  int SetChain(TChain* chain, Int_t Sample,Int_t Nfiles=50);
  int SetChain(TChain* chain, UserInterFace ui);
};

#endif // #ifndef _FILEREAD_HH
