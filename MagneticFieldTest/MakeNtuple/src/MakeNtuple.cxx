#include "MakeNtuple.hh"


int MakeNtuple::CreateNtuple(int nlayer,std::vector<PixelEvent> *event){
  // printing number of trigger for each file                                                          
  TFile * file = new TFile(filename.c_str(),"RECREATE");
  TTree * tree = new TTree(treename.c_str(),treename.c_str());
  int eventnumber;
  int bcid[3];
  // hit
  int nhit[3];
  std::vector<int> hit_iChannel[3];
  std::vector<int> hit_iCol[3];
  std::vector<int> hit_iRow[3];
  std::vector<int> hit_nToT[3];
  std::vector<int> hit_nToT2[3];
  std::vector<int> hit_nLV1[3];
  std::vector<double>  hit_posX[3];
  std::vector<double>  hit_posY[3];
  std::vector<double>  hit_posZ[3];


  // cluster
  int ncluster[3];
  std::vector<int> cl_sumToT[3];
  std::vector<int> cl_sumToT2[3];
  std::vector<double>  cl_posX[3];
  std::vector<double>  cl_posY[3];
  std::vector<double>  cl_posZ[3];


  
  tree->Branch("EventNumber",&eventnumber, "eventnumber/i");

  tree->Branch("BCID_d0",&bcid[0], "BCID_d0/i");
  tree->Branch("BCID_d1",&bcid[1], "BCID_d1/i");
  tree->Branch("BCID_d2",&bcid[2], "BCID_d2/i");

  tree->Branch("NHit_d0", &nhit[0], "NHit_d0/i");
  tree->Branch("hit_iChannel_d0",&hit_iChannel[0]);
  tree->Branch("hit_iCol_d0",&hit_iCol[0]);
  tree->Branch("hit_iRow_d0",&hit_iRow[0]);
  tree->Branch("hit_nToT_d0",&hit_nToT[0]);
  tree->Branch("hit_nToT2_d0",&hit_nToT2[0]);
  tree->Branch("hit_nLV1_d0",&hit_nLV1[0]);
  tree->Branch("hit_posX_d0",&hit_posX[0]);
  tree->Branch("hit_posY_d0",&hit_posY[0]);
  tree->Branch("hit_posZ_d0",&hit_posZ[0]);       
  tree->Branch("NCluster_d0", &ncluster[0], "NCluster_d0/i");
  tree->Branch("cl_sumToT_d0", &cl_sumToT[0]); 
  tree->Branch("cl_sumToT2_d0",&cl_sumToT2[0]);
  tree->Branch("cl_posX_d0",   &cl_posX[0]);   
  tree->Branch("cl_posY_d0",   &cl_posY[0]);   
  tree->Branch("cl_posZ_d0",   &cl_posZ[0]);   

  tree->Branch("NHit_d1", &nhit[1], "NHit_d1/i");
  tree->Branch("hit_iChannel_d1",&hit_iChannel[1]);
  tree->Branch("hit_iCol_d1",&hit_iCol[1]);
  tree->Branch("hit_iRow_d1",&hit_iRow[1]);
  tree->Branch("hit_nToT_d1",&hit_nToT[1]);
  tree->Branch("hit_nToT2_d1",&hit_nToT2[1]);
  tree->Branch("hit_nLV1_d1",&hit_nLV1[1]);
  tree->Branch("hit_posX_d1",&hit_posX[1]);
  tree->Branch("hit_posY_d1",&hit_posY[1]);
  tree->Branch("hit_posZ_d1",&hit_posZ[1]);       
  tree->Branch("NCluster_d1", &ncluster[1], "NCluster_d1/i");
  tree->Branch("cl_sumToT_d1", &cl_sumToT[1]); 
  tree->Branch("cl_sumToT2_d1",&cl_sumToT2[1]);
  tree->Branch("cl_posX_d1",   &cl_posX[1]);   
  tree->Branch("cl_posY_d1",   &cl_posY[1]);   
  tree->Branch("cl_posZ_d1",   &cl_posZ[1]);   

  tree->Branch("NHit_d2", &nhit[2], "NHit_d2/i");
  tree->Branch("hit_iChannel_d2",&hit_iChannel[2]);
  tree->Branch("hit_iCol_d2",&hit_iCol[2]);
  tree->Branch("hit_iRow_d2",&hit_iRow[2]);
  tree->Branch("hit_nToT_d2",&hit_nToT[2]);
  tree->Branch("hit_nToT2_d2",&hit_nToT2[2]);
  tree->Branch("hit_nLV1_d2",&hit_nLV1[2]);
  tree->Branch("hit_posX_d2",&hit_posX[2]);
  tree->Branch("hit_posY_d2",&hit_posY[2]);
  tree->Branch("hit_posZ_d2",&hit_posZ[2]);       
  tree->Branch("NCluster_d2", &ncluster[2], "NCluster_d2/i");
  tree->Branch("cl_sumToT_d2", &cl_sumToT[2]); 
  tree->Branch("cl_sumToT2_d2",&cl_sumToT2[2]);
  tree->Branch("cl_posX_d2",   &cl_posX[2]);   
  tree->Branch("cl_posY_d2",   &cl_posY[2]);   
  tree->Branch("cl_posZ_d2",   &cl_posZ[2]);   

  for(unsigned int ifile=0;ifile<event[0].size();ifile++){
    std::cout << "file : " << ifile << " nevent => " ;
    std::cout << event[0][ifile].Get().size() << " " ;
    std::cout << event[1][ifile].Get().size() << " " ;
    std::cout << event[2][ifile].Get().size() << std::endl;
    std::cout << "first event : " << event[0][ifile].Get()[0]->GetEventNumber() << std::endl;
    unsigned int lastevent[3]={
      event[0][ifile].Get()[event[0][ifile].Get().size()-1]->GetEventNumber(),
      event[1][ifile].Get()[event[1][ifile].Get().size()-1]->GetEventNumber(),
      event[2][ifile].Get()[event[2][ifile].Get().size()-1]->GetEventNumber()
    };
    std::cout << " event until : " << lastevent[0] << " " << lastevent[1] << " " << lastevent[2] << std::endl;
    //    unsigned int endevent=(event[0][ifile].Get().size())>90000?90000:(event[0][ifile].Get().size());
    unsigned int endevent=lastevent[0]<lastevent[1]?lastevent[1]:lastevent[0];
    endevent=endevent<lastevent[2]?lastevent[2]:endevent;
    std::cout << " loop ends at " << endevent << std::endl;
    unsigned int ievely[3]={0,0,0};
    for(unsigned int ieve=1;ieve<endevent;ieve++){
      if(ieve%1000==0)std::cout << "processing : " << ieve << "th event" << std::endl;
      eventnumber=-1;
      for(int ii=0;ii<3;ii++){
	bcid[ii]=0;
	nhit[ii]=0;
	ncluster[ii]=0;
	hit_iChannel[ii].clear();
	hit_iCol[ii].clear();
	hit_iRow[ii].clear();
	hit_nToT[ii].clear();
	hit_nToT2[ii].clear();
	hit_nLV1[ii].clear();
	hit_posX[ii].clear();
	hit_posY[ii].clear();
	hit_posZ[ii].clear();
	cl_sumToT[ii].clear();
	cl_sumToT2[ii].clear();
	cl_posX[ii].clear();
	cl_posY[ii].clear();
	cl_posZ[ii].clear();
      }
      //      std::cout << "check 0 " << ieve << " " << event[0][ifile].Get()[ieve]->GetEventNumber()<< std::endl;
      bool skipevent[3]={false,false,false};
      for(int ly=0; ly<nlayer;ly++){
//	std::cout << ly << " ";
	//	std::cout << event[0][ifile].Get()[ieve]->GetEventNumber() << std::endl;
	//	std::cout << event[ly][ifile].Get()[ieve]->GetEventNumber()<< std::endl;
	//	std::cout << "check 0.3 " << std::endl;

	if(ievely[ly]<event[ly][ifile].Get().size()){
	  if(event[ly][ifile].Get()[ievely[ly]]->GetEventNumber()!=(int)ieve){
	    std::cout << "event number mismatch : ly" << ly << " " << ieve << " "
		      << event[ly][ifile].Get()[ievely[ly]]->GetEventNumber() << std::endl;
	    skipevent[ly]=true;
	  }
	}else{
	    skipevent[ly]=true;
	}
//	if(event[0][ifile].Get()[ieve]->GetEventNumber()
//	   !=event[ly][ifile].Get()[ieve]->GetEventNumber()){
//	  std::cout << " eventnumber miss match : " ;
//	  std::cout << event[0][ifile].Get()[ieve]->GetEventNumber() << " ";
//	  std::cout << event[ly][ifile].Get()[ieve]->GetEventNumber() << std::endl;
//	}
	//	std::cout << "check 0.4 " << std::endl;
      }
      //      std::cout << "check 1 " << std::endl;

      //      eventnumber=event[0][ifile].Get()[ieve]->GetEventNumber();
      eventnumber=ieve;
      if(0){
	std::cout << ieve << " ";
	for(int ly=0;ly<nlayer;ly++)std::cout << event[ly][ifile].Get()[ievely[ly]]->GetEventNumber() << " ";
	std::cout << std::endl;
      }
      for(int ly=0; ly<nlayer;ly++){
	if(skipevent[ly]){
	  std::cout << "skipped event in ly" << ly << " event : " << eventnumber << std::endl;
	  continue;
	}
	AnPixelEvent *anevent=event[ly][ifile].Get()[ievely[ly]];
	bcid[ly]=anevent->GetBCID();
	nhit[ly]=anevent->GetHits().size();
	for(unsigned int ihit=0;ihit<anevent->GetHits().size();ihit++){
	  hit_iChannel[ly].push_back(anevent->GetHits()[ihit].Channel());
	  hit_iCol[ly].push_back(anevent->GetHits()[ihit].Col());
	  hit_iRow[ly].push_back(anevent->GetHits()[ihit].Row());
	  hit_nToT[ly].push_back(anevent->GetHits()[ihit].ToT());
	  hit_nToT2[ly].push_back(anevent->GetHits()[ihit].ToT2());
	  hit_nLV1[ly].push_back(anevent->GetHits()[ihit].LV1());
	  hit_posX[ly].push_back(anevent->GetHits()[ihit].PosX());
	  hit_posY[ly].push_back(anevent->GetHits()[ihit].PosY());
	  hit_posZ[ly].push_back(anevent->GetHits()[ihit].PosZ());
	}
	ncluster[ly]=anevent->GetClusters().size();
	for(unsigned int icl=0;icl<anevent->GetClusters().size();icl++){
	  cl_sumToT[ly].push_back(anevent->GetClusters()[icl].SumToT());
	  cl_sumToT2[ly].push_back(anevent->GetClusters()[icl].SumToT2());
	  cl_posX[ly].push_back(anevent->GetClusters()[icl].PosX());
	  cl_posY[ly].push_back(anevent->GetClusters()[icl].PosY());
	  cl_posZ[ly].push_back(anevent->GetClusters()[icl].PosZ());
	}
	ievely[ly]++;
      }
      tree->Fill();
    }
  }
  tree->Write();
  file->Close();
  return 0;
}
