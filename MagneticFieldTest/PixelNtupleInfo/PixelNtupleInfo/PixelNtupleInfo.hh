//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Dec 17 03:37:52 2015 by ROOT version 5.34/25
// from TTree pixel/pixel
// found on file: ../ntuple/KEK102_KEK103_KEK104_th2400_7ToTat10ke_run002_B0_SOURCE_SCAN.root
//////////////////////////////////////////////////////////

#ifndef PixelNtupleInfo_h
#define PixelNtupleInfo_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <vector>

// Fixed size dimensions of array or collections stored in the TTree if any.

class PixelNtupleInfo {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   UInt_t          EventNumber;
   UInt_t          BCID_d0;
   UInt_t          BCID_d1;
   UInt_t          BCID_d2;
   UInt_t          NHit_d0;
   std::vector<int>     *hit_iChannel_d0;
   std::vector<int>     *hit_iCol_d0;
   std::vector<int>     *hit_iRow_d0;
   std::vector<int>     *hit_nToT_d0;
   std::vector<int>     *hit_nToT2_d0;
   std::vector<int>     *hit_nLV1_d0;
   std::vector<double>  *hit_posX_d0;
   std::vector<double>  *hit_posY_d0;
   std::vector<double>  *hit_posZ_d0;
   UInt_t          NCluster_d0;
   std::vector<int>     *cl_sumToT_d0;
   std::vector<int>     *cl_sumToT2_d0;
   std::vector<double>  *cl_posX_d0;
   std::vector<double>  *cl_posY_d0;
   std::vector<double>  *cl_posZ_d0;
   UInt_t          NHit_d1;
   std::vector<int>     *hit_iChannel_d1;
   std::vector<int>     *hit_iCol_d1;
   std::vector<int>     *hit_iRow_d1;
   std::vector<int>     *hit_nToT_d1;
   std::vector<int>     *hit_nToT2_d1;
   std::vector<int>     *hit_nLV1_d1;
   std::vector<double>  *hit_posX_d1;
   std::vector<double>  *hit_posY_d1;
   std::vector<double>  *hit_posZ_d1;
   UInt_t          NCluster_d1;
   std::vector<int>     *cl_sumToT_d1;
   std::vector<int>     *cl_sumToT2_d1;
   std::vector<double>  *cl_posX_d1;
   std::vector<double>  *cl_posY_d1;
   std::vector<double>  *cl_posZ_d1;
   UInt_t          NHit_d2;
   std::vector<int>     *hit_iChannel_d2;
   std::vector<int>     *hit_iCol_d2;
   std::vector<int>     *hit_iRow_d2;
   std::vector<int>     *hit_nToT_d2;
   std::vector<int>     *hit_nToT2_d2;
   std::vector<int>     *hit_nLV1_d2;
   std::vector<double>  *hit_posX_d2;
   std::vector<double>  *hit_posY_d2;
   std::vector<double>  *hit_posZ_d2;
   UInt_t          NCluster_d2;
   std::vector<int>     *cl_sumToT_d2;
   std::vector<int>     *cl_sumToT2_d2;
   std::vector<double>  *cl_posX_d2;
   std::vector<double>  *cl_posY_d2;
   std::vector<double>  *cl_posZ_d2;

   // List of branches
   TBranch        *b_eventnumber;   //!
   TBranch        *b_BCID_d0;   //!
   TBranch        *b_BCID_d1;   //!
   TBranch        *b_BCID_d2;   //!
   TBranch        *b_NHit_d0;   //!
   TBranch        *b_hit_iChannel_d0;   //!
   TBranch        *b_hit_iCol_d0;   //!
   TBranch        *b_hit_iRow_d0;   //!
   TBranch        *b_hit_nToT_d0;   //!
   TBranch        *b_hit_nToT2_d0;   //!
   TBranch        *b_hit_nLV1_d0;   //!
   TBranch        *b_hit_posX_d0;   //!
   TBranch        *b_hit_posY_d0;   //!
   TBranch        *b_hit_posZ_d0;   //!
   TBranch        *b_NCluster_d0;   //!
   TBranch        *b_cl_sumToT_d0;   //!
   TBranch        *b_cl_sumToT2_d0;   //!
   TBranch        *b_cl_posX_d0;   //!
   TBranch        *b_cl_posY_d0;   //!
   TBranch        *b_cl_posZ_d0;   //!
   TBranch        *b_NHit_d1;   //!
   TBranch        *b_hit_iChannel_d1;   //!
   TBranch        *b_hit_iCol_d1;   //!
   TBranch        *b_hit_iRow_d1;   //!
   TBranch        *b_hit_nToT_d1;   //!
   TBranch        *b_hit_nToT2_d1;   //!
   TBranch        *b_hit_nLV1_d1;   //!
   TBranch        *b_hit_posX_d1;   //!
   TBranch        *b_hit_posY_d1;   //!
   TBranch        *b_hit_posZ_d1;   //!
   TBranch        *b_NCluster_d1;   //!
   TBranch        *b_cl_sumToT_d1;   //!
   TBranch        *b_cl_sumToT2_d1;   //!
   TBranch        *b_cl_posX_d1;   //!
   TBranch        *b_cl_posY_d1;   //!
   TBranch        *b_cl_posZ_d1;   //!
   TBranch        *b_NHit_d2;   //!
   TBranch        *b_hit_iChannel_d2;   //!
   TBranch        *b_hit_iCol_d2;   //!
   TBranch        *b_hit_iRow_d2;   //!
   TBranch        *b_hit_nToT_d2;   //!
   TBranch        *b_hit_nToT2_d2;   //!
   TBranch        *b_hit_nLV1_d2;   //!
   TBranch        *b_hit_posX_d2;   //!
   TBranch        *b_hit_posY_d2;   //!
   TBranch        *b_hit_posZ_d2;   //!
   TBranch        *b_NCluster_d2;   //!
   TBranch        *b_cl_sumToT_d2;   //!
   TBranch        *b_cl_sumToT2_d2;   //!
   TBranch        *b_cl_posX_d2;   //!
   TBranch        *b_cl_posY_d2;   //!
   TBranch        *b_cl_posZ_d2;   //!

   PixelNtupleInfo(TTree *tree=0);
   virtual ~PixelNtupleInfo();
   virtual void     Action(){}
   virtual void     Finalize(){}
   virtual void     Initialize(){}
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef PixelNtupleInfo_cxx
PixelNtupleInfo::PixelNtupleInfo(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../ntuple/KEK102_KEK103_KEK104_th2400_7ToTat10ke_run002_B0_SOURCE_SCAN.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../ntuple/KEK102_KEK103_KEK104_th2400_7ToTat10ke_run002_B0_SOURCE_SCAN.root");
      }
      f->GetObject("pixel",tree);

   }
   Init(tree);
}

PixelNtupleInfo::~PixelNtupleInfo()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t PixelNtupleInfo::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t PixelNtupleInfo::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void PixelNtupleInfo::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   hit_iChannel_d0 = 0;
   hit_iCol_d0 = 0;
   hit_iRow_d0 = 0;
   hit_nToT_d0 = 0;
   hit_nToT2_d0 = 0;
   hit_nLV1_d0 = 0;
   hit_posX_d0 = 0;
   hit_posY_d0 = 0;
   hit_posZ_d0 = 0;
   cl_sumToT_d0 = 0;
   cl_sumToT2_d0 = 0;
   cl_posX_d0 = 0;
   cl_posY_d0 = 0;
   cl_posZ_d0 = 0;
   hit_iChannel_d1 = 0;
   hit_iCol_d1 = 0;
   hit_iRow_d1 = 0;
   hit_nToT_d1 = 0;
   hit_nToT2_d1 = 0;
   hit_nLV1_d1 = 0;
   hit_posX_d1 = 0;
   hit_posY_d1 = 0;
   hit_posZ_d1 = 0;
   cl_sumToT_d1 = 0;
   cl_sumToT2_d1 = 0;
   cl_posX_d1 = 0;
   cl_posY_d1 = 0;
   cl_posZ_d1 = 0;
   hit_iChannel_d2 = 0;
   hit_iCol_d2 = 0;
   hit_iRow_d2 = 0;
   hit_nToT_d2 = 0;
   hit_nToT2_d2 = 0;
   hit_nLV1_d2 = 0;
   hit_posX_d2 = 0;
   hit_posY_d2 = 0;
   hit_posZ_d2 = 0;
   cl_sumToT_d2 = 0;
   cl_sumToT2_d2 = 0;
   cl_posX_d2 = 0;
   cl_posY_d2 = 0;
   cl_posZ_d2 = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_eventnumber);
   fChain->SetBranchAddress("BCID_d0", &BCID_d0, &b_BCID_d0);
   fChain->SetBranchAddress("BCID_d1", &BCID_d1, &b_BCID_d1);
   fChain->SetBranchAddress("BCID_d2", &BCID_d2, &b_BCID_d2);
   fChain->SetBranchAddress("NHit_d0", &NHit_d0, &b_NHit_d0);
   fChain->SetBranchAddress("hit_iChannel_d0", &hit_iChannel_d0, &b_hit_iChannel_d0);
   fChain->SetBranchAddress("hit_iCol_d0", &hit_iCol_d0, &b_hit_iCol_d0);
   fChain->SetBranchAddress("hit_iRow_d0", &hit_iRow_d0, &b_hit_iRow_d0);
   fChain->SetBranchAddress("hit_nToT_d0", &hit_nToT_d0, &b_hit_nToT_d0);
   fChain->SetBranchAddress("hit_nToT2_d0", &hit_nToT2_d0, &b_hit_nToT2_d0);
   fChain->SetBranchAddress("hit_nLV1_d0", &hit_nLV1_d0, &b_hit_nLV1_d0);
   fChain->SetBranchAddress("hit_posX_d0", &hit_posX_d0, &b_hit_posX_d0);
   fChain->SetBranchAddress("hit_posY_d0", &hit_posY_d0, &b_hit_posY_d0);
   fChain->SetBranchAddress("hit_posZ_d0", &hit_posZ_d0, &b_hit_posZ_d0);
   fChain->SetBranchAddress("NCluster_d0", &NCluster_d0, &b_NCluster_d0);
   fChain->SetBranchAddress("cl_sumToT_d0", &cl_sumToT_d0, &b_cl_sumToT_d0);
   fChain->SetBranchAddress("cl_sumToT2_d0", &cl_sumToT2_d0, &b_cl_sumToT2_d0);
   fChain->SetBranchAddress("cl_posX_d0", &cl_posX_d0, &b_cl_posX_d0);
   fChain->SetBranchAddress("cl_posY_d0", &cl_posY_d0, &b_cl_posY_d0);
   fChain->SetBranchAddress("cl_posZ_d0", &cl_posZ_d0, &b_cl_posZ_d0);
   fChain->SetBranchAddress("NHit_d1", &NHit_d1, &b_NHit_d1);
   fChain->SetBranchAddress("hit_iChannel_d1", &hit_iChannel_d1, &b_hit_iChannel_d1);
   fChain->SetBranchAddress("hit_iCol_d1", &hit_iCol_d1, &b_hit_iCol_d1);
   fChain->SetBranchAddress("hit_iRow_d1", &hit_iRow_d1, &b_hit_iRow_d1);
   fChain->SetBranchAddress("hit_nToT_d1", &hit_nToT_d1, &b_hit_nToT_d1);
   fChain->SetBranchAddress("hit_nToT2_d1", &hit_nToT2_d1, &b_hit_nToT2_d1);
   fChain->SetBranchAddress("hit_nLV1_d1", &hit_nLV1_d1, &b_hit_nLV1_d1);
   fChain->SetBranchAddress("hit_posX_d1", &hit_posX_d1, &b_hit_posX_d1);
   fChain->SetBranchAddress("hit_posY_d1", &hit_posY_d1, &b_hit_posY_d1);
   fChain->SetBranchAddress("hit_posZ_d1", &hit_posZ_d1, &b_hit_posZ_d1);
   fChain->SetBranchAddress("NCluster_d1", &NCluster_d1, &b_NCluster_d1);
   fChain->SetBranchAddress("cl_sumToT_d1", &cl_sumToT_d1, &b_cl_sumToT_d1);
   fChain->SetBranchAddress("cl_sumToT2_d1", &cl_sumToT2_d1, &b_cl_sumToT2_d1);
   fChain->SetBranchAddress("cl_posX_d1", &cl_posX_d1, &b_cl_posX_d1);
   fChain->SetBranchAddress("cl_posY_d1", &cl_posY_d1, &b_cl_posY_d1);
   fChain->SetBranchAddress("cl_posZ_d1", &cl_posZ_d1, &b_cl_posZ_d1);
   fChain->SetBranchAddress("NHit_d2", &NHit_d2, &b_NHit_d2);
   fChain->SetBranchAddress("hit_iChannel_d2", &hit_iChannel_d2, &b_hit_iChannel_d2);
   fChain->SetBranchAddress("hit_iCol_d2", &hit_iCol_d2, &b_hit_iCol_d2);
   fChain->SetBranchAddress("hit_iRow_d2", &hit_iRow_d2, &b_hit_iRow_d2);
   fChain->SetBranchAddress("hit_nToT_d2", &hit_nToT_d2, &b_hit_nToT_d2);
   fChain->SetBranchAddress("hit_nToT2_d2", &hit_nToT2_d2, &b_hit_nToT2_d2);
   fChain->SetBranchAddress("hit_nLV1_d2", &hit_nLV1_d2, &b_hit_nLV1_d2);
   fChain->SetBranchAddress("hit_posX_d2", &hit_posX_d2, &b_hit_posX_d2);
   fChain->SetBranchAddress("hit_posY_d2", &hit_posY_d2, &b_hit_posY_d2);
   fChain->SetBranchAddress("hit_posZ_d2", &hit_posZ_d2, &b_hit_posZ_d2);
   fChain->SetBranchAddress("NCluster_d2", &NCluster_d2, &b_NCluster_d2);
   fChain->SetBranchAddress("cl_sumToT_d2", &cl_sumToT_d2, &b_cl_sumToT_d2);
   fChain->SetBranchAddress("cl_sumToT2_d2", &cl_sumToT2_d2, &b_cl_sumToT2_d2);
   fChain->SetBranchAddress("cl_posX_d2", &cl_posX_d2, &b_cl_posX_d2);
   fChain->SetBranchAddress("cl_posY_d2", &cl_posY_d2, &b_cl_posY_d2);
   fChain->SetBranchAddress("cl_posZ_d2", &cl_posZ_d2, &b_cl_posZ_d2);
   Notify();
}

Bool_t PixelNtupleInfo::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void PixelNtupleInfo::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t PixelNtupleInfo::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef PixelNtupleInfo_cxx
