#ifndef __SEABASTLUINFO_HH__
#define __SEABASTLUINFO_HH__

#include <iostream>

class SeabasTLUinfo{
private:
  int m_EventNumber;
  int m_MPPC;
  int m_TrigID;
  int m_TimeStamp;

public:
  SeabasTLUinfo(){}
  SeabasTLUinfo(int evtnum, int *xx);
  ~SeabasTLUinfo(){}
  void SetEventNumber(int en){m_EventNumber=en;}
  void SetMPPC(int mppc){m_MPPC=mppc;}
  void SetTrigID(int tid){m_TrigID=tid;}
  void SetTimeStamp(int tstmp){m_TimeStamp=tstmp;}

  int EventNumber(){return m_EventNumber;}
  int MPPC(){return m_MPPC;}
  int TrigID(){return m_TrigID;}
  int TimeStamp(){return m_TimeStamp;}
};

#endif
