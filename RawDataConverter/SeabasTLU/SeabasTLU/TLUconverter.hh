#ifndef __TLUCONVERTER_HH__
#define __TLUCONVERTER_HH__

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <bitset>
#include <ctime>
#include "SeabasTLUinfo.hh"
#include "TrackHitEvent/TrackHitEvent.hh"
#include "OnlineMonWindow/OnlineMonWindow.hh"
#include "HistogramTools/HistogramVariableStrage.hh"

class TLUconverter{
private:
  int runnumber;
  time_t starttime;
  TrackHitEvent *thisevent;
  std::string filename;
  std::ifstream infile;
  SeabasTLUinfo stlui;
  HistogramVariableStrage *hvs;
  double m_MPPCdeno[4];
  double m_MPPCnume[4];
  int lastTimeStamp;
  int NeInSpill;
  int ROUND;
  int TIMESTAMP;
public:
  TLUconverter(std::string filename);
  ~TLUconverter();
  //void SetThisEvent(TrackHitEvent *_eve);
  void SetThisEvent(TrackHitEvent *_eve){thisevent=_eve;}
  void SetHistogram(HistogramVariableStrage *_hvs);
  void SetHistogram(HistogramVariableStrage *_hvs,OnlineMonWindow *om);
  void FillHistogram();
  int RunNumber(){return runnumber;}
  time_t StartTime(){return starttime;}
  int GetAnEvent(int &eventnum,int *xx);
  int GetEvent(int EventNumber);
  int GetNextEvent();
  SeabasTLUinfo GetTLUinfo(){return stlui;}
  void PrintTLUinfo();
};

#endif
