#include "HSIO2/FEI4EventData.hh"

void FEI4Event::doClustering() {
  // No hits = no cluster
  if (nHits == 0)
    return ;
    
  // Create "copy" of hits
  std::list<FEI4Hit*> unclustered;
  for (unsigned i=0; i<hits.size(); i++)
    unclustered.push_back(&hits[i]);

  // Create first cluster and add first hit
  clusters.push_back(FEI4Cluster());
  clusters.back().addHit(unclustered.front());
  unclustered.erase(unclustered.begin());

  int gap = 1;

  // Loop over vector of unclustered hits until empty
  while (!unclustered.empty()) {
    // Loop over hits in cluster, increases as we go
    for (unsigned i=0; i<clusters.back().nHits; i++) {
      FEI4Hit tHit = *clusters.back().hits[i];
            
      // Loop over unclustered hits
      for (auto j=unclustered.begin(); j!=unclustered.end(); ++j) {
	if ((abs((int)tHit.col - (*j)->col) <= (1+gap))
	    && (abs((int)tHit.row - (*j)->row) <= (1+gap))) {
	  // If not more than 1 pixel gap, add to cluster
	  clusters.back().addHit(*j);
	  unclustered.erase(j--);
	}
      }
    }
    // Still hits to be clustered, create new cluster
    if (!unclustered.empty()) {
      clusters.push_back(FEI4Cluster());
      clusters.back().addHit(unclustered.front());
      unclustered.erase(unclustered.begin());
    }
  }
  // All clustered
        
    
}
