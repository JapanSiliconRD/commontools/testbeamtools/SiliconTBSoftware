#include "SPEC/SPECconverter.hh"

SPECconverter::SPECconverter(std::string filename){
  eventnumber=-1;
  thiseventtag=0;
  file.open(filename, std::fstream::in | std::fstream::binary);
  file.seekg(0, std::ios_base::end);
  int size = file.tellg();
  file.seekg(0, std::ios_base::beg);
  std::cout << "filr : " << filename << " openend" << std::endl;
  std::cout << "Size: " << size/1024.0/1024.0 << " MB" << std::endl;
}
SPECconverter::~SPECconverter(){
  file.close();
}
void SPECconverter::SetHistogram(HistogramVariableStrage *_hvs){
  hvs=_hvs;
  std::stringstream ss ;
  ss.str("");
  ss << "DetDetail/SPEC";
  std::stringstream ss2 ;
  ss2.str("");
  ss2 << "HitMap/SPEC";
  hvs->Add2DHistogram(ss.str()+"/Hitmap",64,0.5,64.5,64,0.5,64.5);
  hvs->AddHistogram(ss.str()+"/TotDist",16,-0.5,15.5);
  hvs->AddHistogram(ss.str()+"/L1id",16,-0.5,15.5);
  hvs->AddHistogram(ss.str()+"/nHits",50,-0.5,49.5);
  hvs->AddHistogram(ss.str()+"/nCluster",30,-0.5,29.5);
  hvs->AddHistogram(ss.str()+"/hitperkeve",10000,0,10e6);
}





void SPECconverter::SetHistogram(HistogramVariableStrage *_hvs,OnlineMonWindow *om){
  hvs=_hvs;
  SetHistogram(hvs);
  std::stringstream ss ;
  ss.str("");
  ss << "DetDetail/SPEC";
  std::stringstream ss2 ;
  ss2.str("");
  ss2 << "HitMap/SPEC";
  for(auto x : hvs->GetHistName()){
    om->registerTreeItem(x);
    om->registerHisto(x,hvs->GetHistogram(x),"HIST",0);
  }
  for(auto y : hvs->Get2DHistName()){
    om->registerTreeItem(y);
    om->registerHisto(y,hvs->Get2DHistogram(y),"COLZ",4);
  }
  om->registerTreeItem(ss2.str()+"Hitmap");
  om->registerHisto(ss2.str()+"Hitmap",hvs->Get2DHistogram(ss.str()+"/Hitmap"),"COLZ",4);

  om->makeTreeItemSummary("DetDetail/SPEC");

}

void SPECconverter::FillHistogram(){
  //  hvs->GetHistogram("SeabasTLU/TriggerID")->Fill(stlui.TrigID());
  //  hvs->GetHistogram("SeabasTLU/TimeStamp")->Fill(stlui.TimeStamp());
  bool debug=false;
  std::stringstream ss ;
  ss.str("");
  ss << "DetDetail/SPEC";

  for( auto x : fe65eve){
    if(x.nHits>50)continue;
    if(x.nHits)hvs->GetHistogram(ss.str()+"/nHits")->Fill(x.nHits);
    if(x.nClusters)hvs->GetHistogram(ss.str()+"/nCluster")->Fill(x.nClusters);
    if(debug)std::cout << "BCID = " << x.bcid << " nHits = " << x.nHits << " nCluster = " << x.nClusters<< std::endl;
    if(x.nHits>0){
      for(auto h : x.hits){
	if(h.col==31&&h.row==62) continue;
//	if(x.bcid-fe65eve[0].bcid==3) continue;
//	if(x.bcid-fe65eve[0].bcid==0) continue;
//	if(x.bcid-fe65eve[0].bcid==1) continue;
//	if(x.bcid-fe65eve[0].bcid==2) continue;
//	if(h.col==21&&h.row==9) continue;
//	if(h.col==23&&h.row==23) continue;
//	if(h.col==11&&h.row==23) continue;
//	if(h.col>=41) continue;
//	if(h.row<=20) continue;
//	if(h.col<=9) continue;
	if(h.tot<3)continue;
	hvs->GetHistogram(ss.str()+"/hitperkeve")->Fill(eventnumber);
	hvs->GetHistogram(ss.str()+"/L1id")->Fill(x.bcid-fe65eve[0].bcid);
	hvs->GetHistogram(ss.str()+"/TotDist")->Fill(h.tot);
	hvs->Get2DHistogram(ss.str()+"/Hitmap")->Fill(h.col,h.row);
	if(debug)std::cout << "L1id=" << x.bcid << "-" << fe65eve[0].bcid << " col = " << h.col << " row=" << h.row << std::endl;
      }
    }
  }
}
int SPECconverter::GetAnEvent(){
  fe65eve.clear();
  if(thiseventtag!=0){
    fe65eve.push_back(lastbcid);
    thiseventtag=lastbcid.tag;
  }
  while(1){
    Fe65Event event;
    event.fromFileBinary(file);
    event.doClustering();
    if (!file)return 0;
    if(fe65eve.size()!=0&&event.tag!=thiseventtag){
      lastbcid=event;
      break;
    }else{
//      if(event.hits.size()>0){
//	if(event.hits.size()==1){
//	  if(!(event.hits[0].col==31&&event.hits[0].row==62)){
//	    event.PrintEvent();
//	  }
//	}else{	
//	  event.PrintEvent();
//	}
//      }

      fe65eve.push_back(event);
      thiseventtag=event.tag;
    }
  }
  int link=1;
  int iFE65layer=0;
  int layer=0;
  for(auto h : hli){
    if(h.type.substr(0,4)=="FE65"){
      iFE65layer++;
      layer=h.layer;
    }
  }
  if(iFE65layer>1)std::cout << "more than one FE65 layer ??" << std::endl;

  for(auto x :fe65eve){
    //    l1id=x.l1id;
    //    bcid=x.bcid;
    for(auto hit : x.hits){
      if(hit.col==31&&hit.row==62) continue;
      PixelHit ahit(0,hit.col,hit.row,hit.tot,0,x.bcid-fe65eve[0].bcid,layer,link,"FE65","KEKFE65-6",true);
      //      if(!(ahit.Col()==31&&ahit.Row()==1)) ahit.PrintPixelHit();
      thisevent->addFE65Hits(ahit);
    }
  }

  eventnumber++;
  return 1;
}
int SPECconverter::GetNextEvent(){
  return GetAnEvent();
}


void SPECconverter::PrintSPECinfo(){
  std::cout << "Event " << eventnumber << " " << std::endl;
  for( auto x : fe65eve){
    std::cout << "l1id " << x.l1id << " " 
	      << "bcid " << x.bcid << " "
	      << "tag " << x.tag << " " 
	      << "nHits " << x.nHits << " " 
	      << "nClusters " << x.nClusters << std::endl;
  }
}
