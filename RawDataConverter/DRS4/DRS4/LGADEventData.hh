#ifndef __LGADEVENTDATA_HH__
#define __LGADEVENTDATA_HH__

class LGADEventData{
 public:
  //  short * gettc(){return tc;}
  //  short ** getchannel(){return channel;}

  //  void setchannel(short **_channel){channel=_channel;}
  //  void setchannel(int index,int tdc,int adc){channel[index][tdc]=adc;}

  int event;
  short tc[4]; // trigger counter bin
  float time[4][1024]; // calibrated time
  short raw[36][1024]; // ADC counts
  short channel[36][1024]; // calibrated input (in V)
  double channelFilter[36][1024]; // calibrated input (in V)
  float xmin[36]; // location of peak
  float base[36]; // baseline voltage
  float amp[36]; // pulse amplitude
  float integral[36]; // integral in a window
  float integralFull[36]; // integral over all bins
  float gauspeak[36]; // time extracted with gaussian fit
  float sigmoidTime[36];//time extracted with sigmoid fit
  float fullFitTime[36];//time extracted with sigmoid fit
  float linearTime0[36]; // constant fraction fit coordinates
  float linearTime15[36];
  float linearTime30[36];
  float linearTime45[36];
  float linearTime60[36];

  float fallingTime[36]; // falling exponential timestamp
    
  float risetime[36]; 
  float constantThresholdTime[36];
  bool _isRinging[36];

};




#endif
