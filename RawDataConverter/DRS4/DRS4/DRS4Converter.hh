#ifndef __DRS4CONVERTER_HH__
#define __DRS4CONVERTER_HH__

// C++ includes
#include <fstream>
#include <string>
#include <iostream>

// ROOT includes
#include <TROOT.h>
#include <TStyle.h>
#include <TFile.h>
#include <TTree.h>
#include <TGraphErrors.h>
#include <TCanvas.h>
#include <iostream>
#include "Common/config.h"
//LOCAL INCLUDES
#include "Aux.hh"
#include "Config.hh"
#include "LGADEventData.hh"
#include "OnlineMonWindow/OnlineMonWindow.hh"
#include "HistogramTools/HistogramVariableStrage.hh"
#include "TrackHitEvent/TrackHitEvent.hh"
#include "Hit/LGADHit.hh"

class DRS4Converter{
 private:
  HistogramVariableStrage *hvs;
  //  std::ifstream fpin;
  FILE * fpin;
  std::string boardNumber;
  Config *config;
  std::string ifileName;
  double tcal_dV[4][1024];
  double dV_sum[4] = {0, 0, 0, 0};
  double tcal[4][1024];
  LGADEventData led;
  // temp variables for data input
  int nGoodEvents;
  double off_mean[4][9][1024];
  TrackHitEvent *thisevent;
  std::vector<DAQLinkInfo> hli;

 public:
  DRS4Converter(){}
  DRS4Converter(std::string inputFileName,std::string configName);
  ~DRS4Converter(){}
  void initialize(std::string inputFileName,std::string configName);
  void ReadBinFile(std::string inputFileName);
  void ReadConfigFile(std::string configName);
  void Calibration();
  void SetThisEvent(TrackHitEvent *_eve);
  void SetDAQInfo(std::vector<DAQLinkInfo>_hli){hli=_hli;}
  void SetHistogram(HistogramVariableStrage *_hvs);
  void SetHistogram(HistogramVariableStrage *_hvs,OnlineMonWindow *om);
  int GetAnEvent();
  int GetEvent(int EventNumber);
  int GetNextEvent();
  void FillThisEvent();
  LGADEventData GetLGADEventData(){return led;}
  void FillHistogram();
  void PrintInfo();

};

#endif
