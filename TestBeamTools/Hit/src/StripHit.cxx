#include <iostream>
#include "StripHit.hh"

void StripHit::SetStripHit(int ich, int icol, int irow,
    int nadc, int nadc2, int Layer,std::string _Type,
    bool rowdata){
  iChannel = ich;
  iCol = icol;
  iRow = irow;
  nADC = nadc;
  nADC2 = nadc2;
  Type = _Type;
  iLayer = Layer;

  double _posX = 0.025+iCol*0.050;
  double _posY = 0.025+iRow*0.050;
  //  Type=="C";
  //  std::cout << shift[0]  << " " << shift[1] << std::endl;
  
  double _posZ=0;
  SetPosX(_posX);
  SetPosY(_posY);
  //  SetPosX(_posX);
  //  SetPosY(_posY);
  SetPosZ(_posZ);
}
