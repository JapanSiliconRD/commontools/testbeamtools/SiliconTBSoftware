#include <iostream>
#include "PixelHit.hh"


void PixelHit::SetPixelHit(int ich,int icol,int irow, int ntot, int ntot2, int lv1,int Layer,int daqlink,std::string _type, std::string _name ,bool rowdata){
  iCol=icol; iRow=irow; iChannel=ich; nToT=ntot; nToT2=ntot2;
  nLV1=lv1; iLayer=Layer; iDAQLink=daqlink; type=_type; name=_name;
  if(type!="FE65"
     &&type!="RD53A"
     &&type!="FEI4SING"
     &&type!="FEI4DOUB"
     &&type!="FEI4QUADA"
     &&type!="FEI4QUADB"
     &&type!="FEI4QUADC")
    std::cerr << "unknown type : " << type << std::endl;
  
  int ncol=0;
  int nrow=0;
  if(type.substr(0,4)=="FEI4"){
    ncol=80;
    nrow=336;
  }else if(type.substr(0,4)=="FE65"){
    ncol=64;
    nrow=64;
  }else if(type.substr(0,4)=="RD53A"){
    ncol=400;
    nrow=192;
  }

  if(type.substr(0,4)=="FEI4"){
    if(rowdata){
      if(ich==4 || ich==0){      // This channel is GA=3 (RJ4)
	iRow=nrow-1-iRow;
      }else if(ich==3){      // This channel is GA=4 (RJ3)
	iCol=ncol+iCol;
	iRow=nrow-1-iRow;
      }else if(ich==2){      // This channel is GA=2 (RJ2)
	iCol=ncol-1-iCol; iCol=80+iCol;
	iRow=nrow+iRow;
      }else if(ich==1){      // This channel is GA=1 (RJ1)
	iCol=ncol-1-iCol;
	iRow=nrow+iRow;
      }
    }
  }
    
  double _posX=0;
  double _posY=0;
  if(type.substr(0,4)=="FE65"){
    _posX = 0.025+iCol*0.050;
    _posY = 0.025+iRow*0.050;
  }

  if(type.substr(0,4)=="FEI4"){
    _posX = 0.125+iCol*0.250;
    _posY = 0.025+iRow*0.050;
  }

  //  type=="C";
  double nSkipRow=0;
  double btwbumpCol=0;
  if(type=="FEI4DOUB"){
    nSkipRow=35;
    btwbumpCol=0;
  }
  if(type=="FEI4QUADA"){
    nSkipRow=8;
    btwbumpCol=0.350;
  }else if(type=="FEI4QUADB"){
    nSkipRow=6;
    btwbumpCol=0.350;
  }else if(type=="FEI4QUADC"){
    nSkipRow=4;
    btwbumpCol=0.250;
  }

  if(iCol>=81)_posX+=(btwbumpCol-0.050);
  if(iCol==79)_posX+=(btwbumpCol-0.050)/4;
  if(iCol==80)_posX+=(btwbumpCol-0.050)*3/4;
  if(iRow>=336)_posY+=nSkipRow*0.050;
  double _posZ=0;
  SetPosX(_posX);
  SetPosY(_posY);
  //  SetPosX(_posX);
  //  SetPosY(_posY);
  SetPosZ(_posZ);
  //  std::cout << "last " << iCol << " " << iRow << std::endl;
}


bool PixelHit::isNext(PixelHit nexthit){
  if(nexthit.GetLayer()!=iLayer)return false;
  if((std::fabs(iCol-nexthit.Col())==0&&std::fabs(iRow-nexthit.Row())==1)
     || (std::fabs(iCol-nexthit.Col())==1&&std::fabs(iRow-nexthit.Row())==0))
    return true;
  else return false; 
}

void PixelHit::PrintPixelHit(){
  std::cout <<" (link, ly ,name ,RJ,col,row,posX,posY,posZ,ntot,ntot2,lv1,type)=(" 
	    << iDAQLink << " " << iLayer << " " << name << " " <<  iChannel <<","<<  iCol <<"," << iRow << "," 
	    << PosX() << "," << PosY() << "," <<PosZ()  << "," 
	    << nToT << "," << nToT2<< "," 
	    << nLV1 << ","  << type<< ")" << std::endl;
}
