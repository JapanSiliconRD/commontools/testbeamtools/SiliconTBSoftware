#include "Configuration/DAQLinkInfo.hh"

DAQLinkInfo::DAQLinkInfo(){
  link=-1; layer=-1; channel=-1; type=""; name="";
}
void DAQLinkInfo::setInfo(DAQLinkInfo a){
  link=a.link;layer=a.layer; channel=a.channel;
  type=a.type; name=a.name;
}
void DAQLinkInfo::setInfo(std::string _name,int _link,int _layer, std::string _type, int _channel){
  name=_name;link=_link; layer=_layer; type=_type; channel=_channel;
}
void DAQLinkInfo::PrintInfo(){
  std::cout << "name,link,layer,type,channel = " 
	    << name << " " 
	    << link << " "
	    << layer << " "
	    << type << " " 
	    << channel << std::endl;
}
