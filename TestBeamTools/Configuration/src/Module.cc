#include "Module.hh"

void Module::SetDefaultModule(){
  FEtype=modtype.substr(0,4);
  nCol=0;
  nRow=0;
  colsize=0;
  rowsize=0;
  if(FEtype=="FEI4"){
    nCol=80;
    nRow=336;
    colsize=0.25;
    rowsize=0.05;
  }else if(FEtype=="FE65"){
    nCol=64;
    nRow=64;
    colsize=0.05;
    rowsize=0.05;
  }
  nSkipRow=0;
  btwbumpCol=0;
  if(modtype=="FEI4QUADA"){
    nSkipRow=8;
    btwbumpCol=0.350;
  }else if(modtype=="FEI4QUADB"){
    nSkipRow=6;
    btwbumpCol=0.350;
  }else if(modtype=="FEI4QUADC"){
    nSkipRow=4;
    btwbumpCol=0.250;
  }

}
