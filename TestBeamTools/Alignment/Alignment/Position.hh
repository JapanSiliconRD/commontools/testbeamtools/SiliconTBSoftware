#ifndef __POSITION__HH__
#define __POSITION__HH__
#include <iostream>
#include <iomanip> 
#include <vector>
#include <cmath>
#include "Align.hh"
#include "TVector3.h"

class Position{
protected:
  int iLayer;
  double sensorX;
  double sensorY;

  double  posX;
  double  posY;
  double  posZ;
  double  posXe;
  double  posYe;
  double  posZe;
  double  glposX;
  double  glposY;
  double  glposZ;
  double  glposXe;
  double  glposYe;
  double  glposZe;

public:
  Position();
  ~Position(){}
  void SetLayer(int lay){iLayer=lay;}
  int GetLayer(){return iLayer;}
  void SetSensorSize(double _senX,double _senY){sensorX=_senX;sensorY=_senY;}
  void SetPosX(double posx){posX=posx;};
  void SetPosY(double posy){posY=posy;};
  void SetPosZ(double posz){posZ=posz;};
  void SetPosXe(double posxe){posXe=posxe;};
  void SetPosYe(double posye){posYe=posye;};
  void SetPosZe(double posze){posZe=posze;};
  void SetGlobalPosX(double posx){glposX=posx;};
  void SetGlobalPosY(double posy){glposY=posy;};
  void SetGlobalPosZ(double posz){glposZ=posz;};
  void SetGlobalPosXe(double posxe){glposXe=posxe;};
  void SetGlobalPosYe(double posye){glposYe=posye;};
  void SetGlobalPosZe(double posze){glposZe=posze;};
  double PosX(){return posX;};
  double PosY(){return posY;};
  double PosZ(){return posZ;};
  double PosXe(){return posXe;};
  double PosYe(){return posYe;};
  double PosZe(){return posZe;};
  TVector3 Pos(){TVector3 v(posX,posY,posZ); return v;}
  //  void SetGlobalPosition(Align ar,int ilayer);
  void SetGlobalPosition(Align ar,std::string name);
  void SetLocalPosition(Align ar,std::string name);
  double GlobalPosX(){return glposX;};
  double GlobalPosY(){return glposY;};
  double GlobalPosZ(){return glposZ;};
  double GlobalPosXe(){return glposXe;};
  double GlobalPosYe(){return glposYe;};
  double GlobalPosZe(){return glposZe;};
  void GetRotatePos(double angle, double &p1,double &p2);
  void PrintPosition();
};

#endif
