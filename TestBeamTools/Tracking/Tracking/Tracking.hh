#ifndef __Tracking_HH__
#define __Tracking_HH__

#include <iostream>
#include "TVector3.h"
#include "TF1.h"
#include "TPolyLine3D.h"
//#include "Module.hh"
#include "TrackInB.hh"
#include "Line3D.hh"

class Tracking : public TrackInB {
private:
  double theta;
  double phi;
  double ChisqX;
  double ChisqY;
  double Chisq3D;
public:
  Tracking();
  ~Tracking(){}
  void SetChisq(double x1,double y1){ChisqX=x1;ChisqY=y1;}
  void SetChisqX(double x1){ChisqX=x1;}
  void SetChisqY(double y1){ChisqY=y1;}
  void SetChisq3D(double chi){Chisq3D=chi;}
  double GetChisqX(){return ChisqX;}
  double GetChisqY(){return ChisqY;}
  double GetChisq3D(){return Chisq3D;}
  void SetThetaPhi(double _theta,double _phi){
    theta=_theta;
    phi=_phi;
  }
  double GetTheta(){return theta;}
  double GetPhi(){return phi;}
};

#endif
