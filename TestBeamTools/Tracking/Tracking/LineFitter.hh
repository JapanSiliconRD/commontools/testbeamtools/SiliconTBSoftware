#ifndef __LineFitter_hh__
#define __LineFitter_hh__

#include <iostream>
#include "LineFitFCN.hh"
//#include "TFitterMinuit.h"
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "Alignment/Position.hh"
class LineFitter {
private:
  std::vector<Position>clusters;
public:
  LineFitter(){};
  ~LineFitter(){};
  int PerformChisqFit(double Bfield,const double *xx,const double *xxe, double &Chisq);
  int PerformTestFit();
  void SetClusterPoints(std::vector<Position>cls){clusters=cls;}

};


#endif
