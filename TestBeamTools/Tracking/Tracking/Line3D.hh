#ifndef __Line3D_HH__
#define __Line3D_HH__

#include <iostream>
#include "TVector3.h"
#include "TF1.h"
#include "TPolyLine3D.h"

class Line3D{
private:
  TVector3 direction; // unit direction vector 
  TVector3 aPoint;  // One point on the 3D line
public:
  Line3D(){}
  ~Line3D(){}
  void SetTwoPoint(TVector3 vec1,TVector3 vec2);
  void SetTwoPoint(double x0,double y0, double z0,double x1,double y1,double z1);
  void SetApoint(TVector3 apoint){aPoint=apoint; }
  void SetApoint(double x0,double y0, double z0){aPoint.SetXYZ(x0,y0,z0);}
  TVector3 GetApoint(){return aPoint;}
  void SetDirection(TVector3 dir){dir.SetMag(1);direction=dir;}
  void SetDirection(double thetax,double thetay);
  TVector3 GetDirection(){return direction;}
  TVector3 GetPointFromZ(double _z);
  std::vector<TVector3> GetPointsOnLine(int npoint,double ipitch,bool doOneside=false);
  void PrintPointsOnLine(int npoint,double ipitch,bool doOneside=false);
  TPolyLine3D * GetPolyLine3D(int npoint,double ipitch,bool doOneside=false);

  double GetDistance (Line3D otherline);
  TVector3 GetCenter (Line3D otherline);
};

#endif
