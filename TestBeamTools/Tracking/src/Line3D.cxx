#include "Line3D.hh"

void Line3D::SetTwoPoint(TVector3 vec1,TVector3 vec2){
  aPoint=vec1; 
  direction=vec2-vec1;
  direction.SetMag(1);
}

void Line3D::SetTwoPoint(double x0,double y0, double z0,double x1,double y1,double z1){
  aPoint.SetXYZ(x0,y0,z0);
  direction.SetXYZ(x1-x0,y1-y0,z1-z0);
  direction.SetMag(1);
}
void Line3D::SetDirection(double thetax,double thetay){
  //  std::cout << thetax << " " << thetay << std::endl;
  TVector3 dir(tan(thetax),tan(thetay),1);
  //  std::cout << dir.X() << " " << dir.Y() << " " << dir.Z()  << std::endl;
  SetDirection(dir);
}
TVector3 Line3D::GetPointFromZ(double _z){
  if(direction.Z()!=0){
    double aa = (_z-aPoint.Z())/direction.Z();
    TVector3 dir;
    dir=direction;
    dir.SetMag(aa);
    return aPoint+dir;
  }else{
    TVector3 zero(0,0,0);
    //    std::cerr << "line is parallel to the plain z=0... " << std::endl;
    return zero;
  }
}
std::vector<TVector3> Line3D::GetPointsOnLine(int npoint,double ipitch,bool doOneside){
  std::vector<TVector3> pol; pol.clear();
  for(int ip=0;ip<npoint;ip++){
    double aa;
    if(doOneside){
      aa=ip*ipitch;
    }else{
      aa=(0-(npoint-1)/2+ip)*ipitch;
    }
    TVector3 dir;
    dir=direction;
      dir.SetMag(aa);
      pol.push_back(aPoint+dir);
  }
  return pol;
}
void Line3D::PrintPointsOnLine(int npoint,double ipitch,bool doOneside){
  std::vector<TVector3> pol; pol.clear();
  pol=GetPointsOnLine(npoint,ipitch,doOneside);
  for(unsigned int ip=0;ip<pol.size();ip++){
    std::cout << ip << " : "<< pol[ip].X() << " " << pol[ip].Y() << " " << pol[ip].Z() << " "  << std::endl;
  }
}
TPolyLine3D * Line3D::GetPolyLine3D(int npoint,double ipitch,bool doOneside){
  std::vector<TVector3> pol; pol.clear();
  pol=GetPointsOnLine(npoint,ipitch,doOneside);
  TPolyLine3D *pl3d = new TPolyLine3D(npoint);
  for(unsigned int ip=0;ip<pol.size();ip++){
    pl3d->SetPoint(ip,pol[ip].X(),pol[ip].Y(),pol[ip].Z());
  }
  return pl3d;
}

double Line3D::GetDistance (Line3D otherline){
  TVector3 P1=aPoint;
  TVector3 v1=direction;
  TVector3 P2=otherline.GetApoint();
  TVector3 v2=otherline.GetDirection();

  double Dv=v1.Dot(v2);
  double D1=v1.Dot(P2-P1);
  double D2=v2.Dot(P2-P1);

  double t1=(D1-D2*Dv)/(1-Dv*Dv);
  double t2=(D2-D1*Dv)/(Dv*Dv-1);
  v1.SetMag(t1);
  v2.SetMag(t2);
  
  TVector3 btw=(P1+v1)-(P2+v2);
  return btw.Mag();
}

TVector3 Line3D::GetCenter (Line3D otherline){
  TVector3 P1=aPoint;
  TVector3 v1=direction;
  TVector3 P2=otherline.GetApoint();
  TVector3 v2=otherline.GetDirection();

  double Dv=v1.Dot(v2);
  double D1=v1.Dot(P2-P1);
  double D2=v2.Dot(P2-P1);

  double t1=(D1-D2*Dv)/(1-Dv*Dv);
  double t2=(D2-D1*Dv)/(Dv*Dv-1);

  v1.SetMag(t1);
  v2.SetMag(t2);
  TVector3 btw=(P1+v1)-(P2+v2);
  if(btw.Mag()!=0) btw.SetMag(btw.Mag()/2);
  return P2+v2+btw;
}
