#include "TGraph.h"
#include "TCanvas.h"
#include "TrackInB.hh"

TVector3 TrackInB::GetPointFromZinB(double z){
  if(Bfield==0)return GetPointFromZ(z);
  TVector3 linepos=GetPointFromZ(z);
  double thetax=atan2(GetDirection().X(),GetDirection().Z());
  //  std::cout << GetDirection().X() << " " << GetDirection().Z() << std::endl;
  //  std::cout << thetax << std::endl;
  double rho=pT/0.3/Bfield;
  double zz=z/cos(thetax);
  double zrho=z-rho*sin(thetax);
  double corr=sqrt(zz*zz+rho*rho-zrho*zrho)-sqrt(rho*rho-zrho*zrho);
  TVector3 Zero(-99,-99,-99);
  if(rho<zrho)return Zero;
  TVector3 trackpos(linepos.X()+corr,linepos.Y(),linepos.Z());
  return trackpos;
}


std::vector<TVector3> TrackInB::GetPointsOnLineInB(int npoint,double startz,double endz){
  if(npoint<2){
    std::cout << "automatically set to valid npoint = 2" << std::endl;
    npoint=2;
  }
  std::vector<TVector3> pol; pol.clear();
  for(int ip=0;ip<npoint;ip++){
    pol.push_back(GetPointFromZinB((endz-startz)*ip/(npoint-1)+startz));
  }
  return pol;
}
void TrackInB::PrintPointsOnLineInB(int npoint,double startz,double endz){
  std::vector<TVector3> pol; pol.clear();
  std::vector<double> polx; polx.clear();
  std::vector<double> polz; polz.clear();
  pol=GetPointsOnLineInB(npoint,startz,endz);
  
  for(unsigned int ip=0;ip<pol.size();ip++){
    polx.push_back(pol[ip].X());
    polz.push_back(pol[ip].Z());
    std::cout << ip << " : "<< pol[ip].X() << " " << pol[ip].Y() << " " << pol[ip].Z() << " "  << std::endl;
  }
  TCanvas *c1 = new TCanvas("c1","c1",500,500);
  TGraph *gr=new TGraph(pol.size(),&polx[0],&polz[0]);
  gr->Draw("AP");
  c1->Update();
  c1->Print("test.png");
  delete c1;
  delete gr;
}
