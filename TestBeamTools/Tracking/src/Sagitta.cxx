#include "Sagitta.hh"

void Sagitta::SetClusters(std::vector<Cluster> clu){
  clusters.clear();
  for(unsigned int iclu=0;iclu<clu.size();iclu++){
    clusters.push_back(clu[iclu].Pos());
  }
}
void Sagitta::CalculateMomentum(){
  if(clusters.size()!=3)return;
  for(unsigned int iclu=0;iclu<clusters.size();iclu++){
    clusters[iclu].SetX(0);
  }
  double L=(clusters[0]-clusters[2]).Mag();

  double l0=(clusters[2]-clusters[0]).Dot(clusters[1]-clusters[0]) / (clusters[2]-clusters[0]).Mag();
  double l1=(clusters[1]-clusters[0]).Mag();
  double s=sqrt(l1*l1-l0*l0);
  double rho=((L/2)*(L/2)-s*s)/(2*s)/1000;
  double slope1=(clusters[1].Z()-clusters[0].Z())/(clusters[1].Y()-clusters[0].Y());
  double slope2=(clusters[2].Z()-clusters[0].Z())/(clusters[2].Y()-clusters[0].Y());
  double sign=0;
  if(slope1<slope2)sign=-1;
  else sign=1;
  pT=0.3*Bfield*rho*sign;
}
