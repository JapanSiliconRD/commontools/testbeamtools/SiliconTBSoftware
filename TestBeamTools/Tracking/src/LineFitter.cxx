#include "LineFitter.hh"
//#include "TFitterMinuit.h"

int LineFitter::PerformChisqFit(double Bfield,const double *xx,const double *xxe,double &Chisq){
  
  //  TFitterMinuit * minuit = new TFitterMinuit();  

  ROOT::Math::Minimizer* minuit =  ROOT::Math::Factory::CreateMinimizer("Minuit2", "");
  minuit->SetMaxFunctionCalls(1000000); // for Minuit/Minuit2


  LineFitFCN lffcn(0,Bfield,clusters);
  ROOT::Math::Functor f(lffcn,5);
  minuit->SetFunction(f);
  // starting values 
  // starting values 
  double startX = clusters.size()>0?clusters[0].GlobalPosX():0; 
  double startY = clusters.size()>0?clusters[0].GlobalPosY():0; 
  double startthetax=0;
  double startthetay=0;
  double startpt=100.;
  // if not limited (vhigh <= vlow) 
  minuit->SetVariable(0,"x",startX,0.001/*,-20,20*/);
  minuit->SetVariable(1,"y",startY,0.001/*,-20,20*/);
  minuit->SetVariable(2,"thetax",startthetax,0.1/*,-1,1*/);
  minuit->SetVariable(3,"thetay",startthetay,0.1/*,-1,1*/);
  minuit->SetFixedVariable(2,"thetax",0);
  minuit->SetFixedVariable(3,"thetay",0);
  if(Bfield==0){
    //    minuit->SetVariable(4,"pT",0,0,0,0);
    minuit->SetFixedVariable(4,"pT",0);
  }else{
    minuit->SetVariable(4,"pT",startpt,10/*,-1010,1010*/);
    
    //    minuit->SetParameter(4,"pT",100000,100000,-100000,100000);
    //    minuit->FixParameter(4);
  }
  minuit->SetPrintLevel(10);
  // create Minimizer (default is Migrad)
  //  minuit->CreateMinimizer();
  int iret = minuit->Minimize();
  if (iret != 0) { 
    return iret; 
  }
  xx=minuit->X();
  xxe=minuit->Errors();
  std::vector<double>xxv;xxv.clear();
  for(int ipar=0;ipar<5;ipar++){
    xxv.push_back(xx[ipar]);
  }
  Chisq=lffcn.GetChisq(xxv);
  return 0;
}


int LineFitter::PerformTestFit(){

  ROOT::Math::Minimizer* minuit =  ROOT::Math::Factory::CreateMinimizer("Minuit2", "");
  minuit->SetMaxFunctionCalls(1000000); // for Minuit/Minuit2

  LineFitFCN lffcn;
  ROOT::Math::Functor f(lffcn,5);
  minuit->SetFunction(f);

  // starting values 
  double startX = -1.2; 
  double startY = 1.0;
  // if not limited (vhigh <= vlow) 
  minuit->SetVariable(0,"x",startX,0.1);
  minuit->SetVariable(1,"y",startY,0.1);
  minuit->SetPrintLevel(3);
  // create Minimizer (default is Migrad)
  //  minuit->CreateMinimizer();
  int iret = minuit->Minimize();
  if (iret != 0) { 
    return iret; 
  }
  std::cout << "\nTest performances........\n\n"; 
  // test performances 
  int nMin = 10000; 
  TStopwatch w; 
  w.Start();
  for (int i = 0; i < nMin; ++i) { 
    minuit->Clear();
    // reset -everything
    //minuit->SetMinuitFCN(&fcn); 
    minuit->SetVariable(0,"x",startX,0.1);
    minuit->SetVariable(1,"y",startY,0.1);
    minuit->SetPrintLevel(0);
    // create Minimizer (default is Migrad)
    //    minuit->CreateMinimizer();
    iret = minuit->Minimize();
    if (iret != 0) { 
      std::cout << "Minimization failed - exit " ; 
      return iret; 
    }
  }
  w.Stop();
  std::cout << "\nTime: \t" << w.RealTime() << " , " << w.CpuTime() << std::endl;  
  return 0;
}
