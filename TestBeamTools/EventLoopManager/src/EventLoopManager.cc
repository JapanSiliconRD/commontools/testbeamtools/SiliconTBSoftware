#include "EventLoopManager.hh"
#include "Common/config.h"
EventLoopManager::EventLoopManager(){
  daqsystem.clear();
  daqsyslist.clear();
  daqsyslist.push_back("SeabasTLU");
  //  daqsyslist.push_back("SVX4");
  //  daqsyslist.push_back("SPEC");
  daqsyslist.push_back("XpressK7");
  daqsyslist.push_back("HSIO2");
  daqsyslist.push_back("DRS4");
  isOnlineMonitor=false;
}

void EventLoopManager::Initialize(){
  std::string runconf=std::getenv("SISOFTDIR"); 
  if(isOnlineMonitor){
    std::cout << "===========================================" << std::endl;
    
    std::cout << "       Welcome to Online Monitor " << std::endl;
    std::cout << "===========================================" << std::endl;
  }else{
    std::cout << "===========================================" << std::endl;
    
    std::cout << "       Welcome to EventLoop Manager " << std::endl;
    std::cout << "===========================================" << std::endl;
  }
  std::cout << "Initializing..." << std::endl;
  
  numevent = new TH1D("numevent","numevent",100,0,10000);
  numevent->SetMinimum(0);
  runnumber = new TH1D("runnumber","runnumber",5000,-0.5,4999.5);
  runnumber->SetMinimum(0);
  //  geometory = new TH3D("geometory","geometory",50,-25,25,50,-25,25,1500,-50,1450);

  isfirstHSIO2event=true;
  isfirstSVX4event=true;
  isfirstAnalysis=true;
  isfirstDRS4event=true;

  if(isOnlineMonitor){
    onlinemon = new OnlineMonWindow(gClient->GetRoot(),1200,800);
    if(onlinemon==NULL){
      std::cerr << "Error Allocating OnlineMonWindow" << std::endl;
      exit(-1);
    }
    
    onlinemon->setRootFileName("test.root");
    onlinemon->AutoReset();
    onlinemon->setReduce(1);
    onlinemon->setUpdate(1000);
    
    //  onlinemon->addTreeItemSummary("HeaderInfo", "numevent");
    onlinemon->registerTreeItem("HeaderInfo/numevent");
    onlinemon->registerHisto("HeaderInfo/numevent",numevent,"",0);
    onlinemon->registerTreeItem("HeaderInfo/runnumber");
    onlinemon->registerHisto("HeaderInfo/runnumber",runnumber,"",0);
    onlinemon->makeTreeItemSummary("HeaderInfo");
  }

  // To be moved to ConfigureManager
  DAQLinkInfo tmphli;
  //  tmphli.setInfo("KEK112",0,2,"FEI4QUADB",2);
  //  hli.push_back(tmphli);
  //  tmphli.setInfo("KEK112",1,2,"FEI4QUADB",3);
  //  hli.push_back(tmphli);

  int il=0;
  tmphli.setInfo("KEK132",10,il++,"FEI4DOUB",0);
  hli.push_back(tmphli);
  tmphli.setInfo("KEK133",6,il,"FEI4DOUB",0);
  hli.push_back(tmphli);
  tmphli.setInfo("KEK133",7,il++,"FEI4DOUB",1);
  hli.push_back(tmphli);

  tmphli.setInfo("KEK53-14",0,il++,"RD53A",0);
  hli.push_back(tmphli);
  tmphli.setInfo("KEK53-8",1,il++,"RD53A",0);
  hli.push_back(tmphli);
//  tmphli.setInfo("KEK53-13",2,il++,"RD53A",0);
//  hli.push_back(tmphli);

  bool isDRS4included=false;
  for(auto x : daqsystem){
    if(x.second=="DRS4")isDRS4included=true;
  }
  if(isDRS4included){
    tmphli.setInfo("LGAD50D",0,il++,"LGADstrip",0);
    hli.push_back(tmphli);
    tmphli.setInfo("LGAD2x2s1",1,il++,"LGAD2x2",0);
    hli.push_back(tmphli);
    tmphli.setInfo("LGAD2x2s2",2,il++,"LGAD2x2",0);
    hli.push_back(tmphli);
  }
  
  tmphli.setInfo("KEK114",12,il,"FEI4QUADB",1);
  hli.push_back(tmphli);
  tmphli.setInfo("KEK114",14,il,"FEI4QUADB",2);
  hli.push_back(tmphli);
  tmphli.setInfo("KEK114",15,il,"FEI4QUADB",3);
  hli.push_back(tmphli);
  tmphli.setInfo("KEK114",0,il++,"FEI4QUADB",4);
  hli.push_back(tmphli);
  tmphli.setInfo("KEK134",8,il,"FEI4DOUB",0);
  hli.push_back(tmphli);
  tmphli.setInfo("KEK134",9,il++,"FEI4DOUB",1);
  hli.push_back(tmphli);
  tmphli.setInfo("KEK141",11,il++,"FEI4DOUB",0);
  hli.push_back(tmphli);
  tmphli.setInfo("KEK142",4,il,"FEI4DOUB",0);
  hli.push_back(tmphli);
  tmphli.setInfo("KEK142",5,il++,"FEI4DOUB",1);
  hli.push_back(tmphli);
  tmphli.setInfo("KEK144",2,il,"FEI4DOUB",0);
  hli.push_back(tmphli);
  tmphli.setInfo("KEK144",3,il++,"FEI4DOUB",1);
  hli.push_back(tmphli);
  //  std::sort(hli.begin(),hli.end(),std::less<DAQLinkInfo>());

  std::vector<int> layers;
  for(auto bb : hli){
    //	std::cout << bb.name << " "<<bb.type<< " " <<  bb.layer << std::endl;
    bool exists=false;
    for(auto cc : layers){
      if(bb.layer==cc){
	exists=true;
	break;
      }
    }
    layers.push_back(bb.layer);
    if(!exists)laylist.push_back(bb);
  }
  
  HistogramVariableStrage *hvs_xpress;
  int iXpressK7=0;
  int nXpressK7=XpressK7datafile.size();
  for(auto x : daqsystem){
    if(x.second=="SeabasTLU"){
      HistogramVariableStrage *hvs_stlu = new HistogramVariableStrage("SeabasTLU");
      std::cout << "===" << std::endl;
      std::cout << "Reading Seabas TLU header information..." << std::endl;
      tlucon=new TLUconverter(TLUdatafile);
      if(isOnlineMonitor){
	tlucon->SetHistogram(hvs_stlu,onlinemon);
	onlinemon->setRunNumber(tlucon->RunNumber());
      }else{
	tlucon->SetHistogram(hvs_stlu);
      }
      runnumber->Fill(tlucon->RunNumber());
      time_t tm=tlucon->StartTime();
      std::cout << "Start Time : " << ctime(&tm) ;
      std::cout << "RunNumber : " << tlucon->RunNumber() << std::endl;
      hvsv.push_back(hvs_stlu);
    } else   if(x.second=="SPEC"){
      HistogramVariableStrage *hvs_spec = new HistogramVariableStrage("SPEC");
      std::cout << "===" << std::endl;
      std::cout << "Reading SPEC header information..." << std::endl;
      speccon=new SPECconverter(SPECdatafile);
      speccon->SetDAQInfo(hli);
      if(isOnlineMonitor){
	speccon->SetHistogram(hvs_spec,onlinemon);
      }else{
	speccon->SetHistogram(hvs_spec);
      }
      hvsv.push_back(hvs_spec);
    } else   if(x.second=="XpressK7"){
      if(iXpressK7==0)hvs_xpress = new HistogramVariableStrage("XpressK7");
      std::cout << "===" << std::endl;
      std::cout << "Reading XpressK7 header information..." << std::endl;
      XpressK7converter *xcon=new XpressK7converter(XpressK7datafile[iXpressK7],x.first);
      xcon->SetDAQInfo(hli);
      if(isOnlineMonitor){
	xcon->SetHistogram(hvs_xpress,onlinemon);
      }else{
	xcon->SetHistogram(hvs_xpress);
      }
      xpresscon.push_back(xcon);
      if(iXpressK7==nXpressK7)hvsv.push_back(hvs_xpress);
      iXpressK7++;
    } else   if(x.second=="HSIO2"){
      std::cout << "===" << std::endl;
      std::cout << "Reading HSIO2 header information..." << std::endl;
      hsio2con=new HSIO2converter(HSIO2datafile);
      hsio2con->SetHSIO2Info(hli);

    }else   if(x.second=="SVX4"){
      HistogramVariableStrage *hvs_svx= new HistogramVariableStrage("SVX4");
      std::cout << "===" << std::endl;
      std::cout << "Reading SVX4 header information..." << std::endl;
      svxcon=new SVXconverter(SVX4datafile);
      hvsv.push_back(hvs_svx);
    }else   if(x.second=="DRS4"){
      HistogramVariableStrage *hvs_drs= new HistogramVariableStrage("DRS4");
      std::cout << "===" << std::endl;
      std::cout << "Reading DRS4 header information..." << std::endl;

      std::stringstream configFile;configFile.str("");
      //      configFile << PACKAGE_SOURCE_DIR<<"/RawDataConverter/DRS4/share/15may2017.config";
      configFile << PACKAGE_SOURCE_DIR<<"/RawDataConverter/DRS4/share/FNALTB201902.config";

      drscon=new DRS4Converter(DRS4datafile,configFile.str());
      drscon->SetDAQInfo(hli);
      hvsv.push_back(hvs_drs);
    }
  }
}

void EventLoopManager::Execute(){
  int nevent = 0;
  bool isend=false;
  bool doprint=false;

  //bool isNeedEff=false;
  while(!isend){
    TrackHitEvent *thisevent=new TrackHitEvent();
    thisevent->setEventNumber(nevent);
    thisevent->setDAQLinkInfo(hli);
    //    ar0.SetAlignmentFile("");
    thisevent->setAlign(ar0);
    if(nevent%1000==0) std::cout<< "==================== EVENT "<< nevent << " ====================" << std::endl;
    //    if(nevent%1==0) std::cout<< "==================== EVENT "<< nevent << " ====================" << std::endl;
    if(isOnlineMonitor){
      onlinemon->UpdateStatus("Getting data..");
      onlinemon->setEventNumber(nevent);
    }
    numevent->Fill(nevent);
    //    numevent->Fill(nevent%10000);
    int iXpressK7=0;
    int nXpressK7=XpressK7datafile.size();
    for(auto x : daqsystem){
      if(x.second=="SeabasTLU"){
	if(0)std::cout << "in execute SeabasTLU " << std::endl;
		tlucon->SetThisEvent(thisevent);
	if(!tlucon->GetNextEvent()){
	  isend=true;
	  std::cout << "TLUdata reached to the end. "  <<  std::endl;
	  std::cout << "number of event is : " << nevent << std::endl;
	}
	
	if(doprint){
	  std::cout << "     TLUinfo  : ";
	  tlucon->PrintTLUinfo();
	}
	tlucon->FillHistogram();
      } else if(x.second=="SPEC"){
	if(0)std::cout << "in execute SPEC " << std::endl;
	speccon->SetThisEvent(thisevent);
	int nskip=thisevent->getnSkipbyHSIO2();
	if(nskip!=0)std::cout << nskip << std::endl;
	while(nskip>0){
	  speccon->GetNextEvent();
	  nskip--;
	}
	if(!speccon->GetNextEvent()){
	  isend=true;
	  std::cout << "SPECdata reached to the end. "  <<  std::endl;
	  std::cout << "number of event is : " << nevent << std::endl;
	}
	thisevent->doFE65Clustering();
	if(doprint){
	  std::cout << "     SPECinfo : ";
	  speccon->PrintSPECinfo();
	}
	speccon->FillHistogram();
      } else if(x.second=="XpressK7"){
	if(0)std::cout << "in execute XpressK7 " << std::endl;
	xpresscon[iXpressK7]->SetThisEvent(thisevent);
	int nskip=thisevent->getnSkipbyHSIO2();
	if(nskip!=0)std::cout << nskip << std::endl;
	while(nskip>0){
	  xpresscon[iXpressK7]->GetNextEvent();
	  nskip--;
	}
	if(!xpresscon[iXpressK7]->GetNextEvent()){
	  isend=true;
	  std::cout << "XpressK7 data reached to the end. "  <<  std::endl;
	  std::cout << "number of event is : " << nevent << std::endl;
	}
	//	if(nevent>8400&&nevent<8500)xpresscon[iXpressK7]->PrintXpressK7info();
	thisevent->doFE65Clustering();
	if(doprint){
	  std::cout << "     XpressK7info : ";
	  xpresscon[iXpressK7]->PrintXpressK7info();
	}
	xpresscon[iXpressK7]->FillHistogram();
	iXpressK7++;
      } else if(x.second=="HSIO2"){
	if(0)std::cout << "in execute HSIO2 " << std::endl;
	hsio2con->SetThisEvent(thisevent);
	if(!hsio2con->GetNextEvent()){
	  isend=true;
	  std::cout << "HSIO2data reached to the end. "  <<  std::endl;
	  std::cout << "number of event is : " << nevent << std::endl;
	}
	if(isfirstHSIO2event){
	  HistogramVariableStrage *hvs_hsio= new HistogramVariableStrage("HSIO2");
	  if(isOnlineMonitor){
	    hsio2con->SetHistogram(hvs_hsio,onlinemon);
	  }else{
	    hsio2con->SetHistogram(hvs_hsio);
	  }
	  hvsv.push_back(hvs_hsio);
	  isfirstHSIO2event=false;
	}
	thisevent->doFEI4Clustering();
	hsio2con->FillHistogram();
	if(doprint){
	  std::cout << "     HSIO2 info : " <<std::endl;;
	  hsio2con->PrintInfo();
	}
      } else if(x.second=="DRS4"){
        //      if(nevent==0)hsio2con->GetNextEvent();
        if(0)std::cout << "in execute DRS4 " << std::endl;
	drscon->SetThisEvent(thisevent);
	if(!drscon->GetNextEvent()){
	  isend=true;
	  std::cout << "DRS4 data reached to the end. "  <<  std::endl;
	  std::cout << "number of event is : " << nevent << std::endl;
        }
	drscon->FillThisEvent();
        if(isfirstDRS4event){
          HistogramVariableStrage *hvs_drs= new HistogramVariableStrage("DRS4");
	  if(isOnlineMonitor){
            drscon->SetHistogram(hvs_drs,onlinemon);
          }else{
            drscon->SetHistogram(hvs_drs);
          }
          hvsv.push_back(hvs_drs);
          isfirstDRS4event=false;
        }
        drscon->FillHistogram();
	//	drscon->PrintInfo();
        if(doprint){
	  std::cout << "     DRS4 info : " <<std::endl;;
          drscon->PrintInfo();
        }
        if(0)std::cout << "done DRS4 " << std::endl;
	
      } else if(x.second=="SVX4"){
	if(0)std::cout << "in execute SVX4 " << std::endl;
	svxcon->SetThisEvent(thisevent);
	if(!svxcon->GetNextEvent()){
	  isend=true;
	  std::cout << "SVX4 data reached to the end. "  <<  std::endl;
	  std::cout << "number of event is : " << nevent << std::endl;
	}
	if(isfirstSVX4event){
	  HistogramVariableStrage *hvs_svx= new HistogramVariableStrage("SVX");
	  if(isOnlineMonitor){
	    svxcon->SetHistogram(hvs_svx,onlinemon);
	  }else{
	    svxcon->SetHistogram(hvs_svx);
	  }
	  hvsv.push_back(hvs_svx);
	  isfirstSVX4event=false;
	}
	svxcon->FillHistogram();
	if(0){
	  std::cout << "     SVX4info : ";
	  svxcon->PrintEvent();
	}
      }
    }

    if(isOnlineMonitor){
      if(nevent==0){
	onlinemon->makeTreeItemSummary("HitMap");
	//	onlinemon->makeTreeItemSummary("DetDetail");
      }
      if(isend){
	isend=false;
	sleep(60);
	continue;
	//	return 0;
      }
    }
    
    if(isfirstAnalysis==true){
      corch = new CorrelationChecker();
      corch->SetHSIO2Info(hli);
      HistogramVariableStrage *hvs_corr= new HistogramVariableStrage("Correlation");
      if(isOnlineMonitor){
	corch->SetHistogram(hvs_corr,onlinemon);
      }else{
	corch->SetHistogram(hvs_corr);
      }
      hvsv.push_back(hvs_corr);
      trhitmker = new TrackHitMaker();
      trhitmker->setNLayer(7); 
      HistogramVariableStrage *hvs_trhm= new HistogramVariableStrage("TrackHitMaker");
      trhitmker->SetHistogram(hvs_trhm);
      hvsv.push_back(hvs_trhm);
 
      isfirstAnalysis=false;
    }
    corch->SetThisEvent(thisevent);
    corch->FillHistogram();
    
    if(isOnlineMonitor){
      onlinemon->increaseAnalysedEventsCounter();
    }else{
      
      bool isGoodTrack=true; 
      int nlay=0;
      for(auto aa : thisevent->getFEI4Clustering()){
	if(aa.size()<0)isGoodTrack=false;
	else nlay++;
      }
      if(nlay!=laylist.size())isGoodTrack=false;
      if(0){
	if(isGoodTrack) std::cout << " one cluster in each layer" << std::endl;
	else std::cout << "not good track" << std::endl;
      }
      if(isGoodTrack){
	std::vector <Position>_pos;
	for(auto bb : laylist){
	  if(bb.type.substr(0,4)=="FEI4"){
	    _pos.push_back(thisevent->getFEI4Clustering(bb.layer)[0]);
	  }
	}
	trhitmker->SetHitPosition(_pos);

	// Alignment
	trhitmker->do2DTracking(nevent>100?0:nevent,0.1);
	
	trhitmker->FillHistogram();
      }
    }

    nevent++;
    delete thisevent;
  }
}

//void EventLoopManager::OnlineMonThread(){
//  Execute();
//}

void EventLoopManager::AddDAQsystem(std::string daq, std::string filename, int boadnum){
  bool ismatched=false;
  for(auto x : daqsyslist){
    if(x==daq)ismatched=true;
  }
  if(ismatched){
    //    daqsystem.push_back(daq);
    daqsystem.push_back(std::make_pair(boadnum,daq));
    if(daq=="SeabasTLU"){
      TLUdatafile=filename;
    }else if(daq=="SPEC"){
      SPECdatafile=filename;
    } else if(daq=="XpressK7"){
      XpressK7datafile.push_back(filename);
    } else if(daq=="HSIO2"){
      HSIO2datafile=filename;
    }else if(daq=="SVX4") {
      SVX4datafile=filename;
    }else if(daq=="DRS4"){
      DRS4datafile=filename;
    }
  }else{
    std::cerr << "no matching DAQ system : " << daq << std::endl;
    std::cerr << "please select from following list" << std::endl;
    for(auto x : daqsyslist)std::cerr << "    " << x << std::endl;
  }
}
EventLoopManager::~EventLoopManager(){
  //  m_thread->join();
}
