#ifndef __CorrelationChecker__HH__
#define __CorrelationChecker__HH__

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include "Hit/PixelHit.hh"
#include "Hit/StripHit.hh"
#include "Configuration/DAQLinkInfo.hh" 
#include "TrackHitEvent/TrackHitEvent.hh"
#include "OnlineMonWindow/OnlineMonWindow.hh"
#include "HistogramTools/HistogramVariableStrage.hh"

class CorrelationChecker {
private:
  TrackHitEvent *event;
  OnlineMonWindow *om;
  HistogramVariableStrage *hvs;
  std::vector<DAQLinkInfo> hli;
  
public:
  CorrelationChecker(){
  }
  ~CorrelationChecker(){}
  void SetHSIO2Info(std::vector<DAQLinkInfo> _hli){hli=_hli;}
  void SetThisEvent(TrackHitEvent *eve){event=eve;}
  void SetHistogram(HistogramVariableStrage *_hvs);
  void SetHistogram(HistogramVariableStrage *_hvs,OnlineMonWindow *om);
  void FillHistogram();
  std::vector<std::vector<PixelHit>>getFEI4HitsWithFE65Hits();
  void PrintCorrelation();
};


#endif
