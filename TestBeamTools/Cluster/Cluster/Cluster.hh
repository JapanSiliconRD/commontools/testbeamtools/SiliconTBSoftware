#ifndef __Cluster_HH__
#define __Cluster_HH__

#include <iostream>
#include <vector>
#include "Alignment/Position.hh"
#include "Hit/PixelHit.hh"

class Cluster : public Position{
private:
  std::vector<PixelHit> hitsforCluster;
  int sumToT;
  int sumToT2;
public:
  Cluster(){
    hitsforCluster.clear();iLayer=0;
    posXe=0.25/sqrt(12);
    posYe=0.05/sqrt(12);
    posZe=1;
  }
  ~Cluster(){}
  int SumToT(){return sumToT;}
  int SumToT2(){return sumToT2;}

  int GetClusterSize(int axis=0);
  std::vector<PixelHit> GetHitsForCluster(){
    return hitsforCluster;
  }
  void AddHitsForCluster(PixelHit hit){
    hitsforCluster.push_back(hit);
  }
  void SetHitsForCluster(std::vector<PixelHit> hits){
    for(unsigned int ihit=0;ihit<hits.size();ihit++){
      hitsforCluster.push_back(hits[ihit]);
    }
  }
  void GetClusterPosition(int type);
  void PrintCluster(){
    std::cout << " (posX,posY,posZ,sumtot,sumtot2)=(" 
	      << posX <<"," << posY << "," 
	      << posZ << "," << sumToT<< "," 
	      << sumToT2 << ")" << std::endl;
  }
};

#endif
