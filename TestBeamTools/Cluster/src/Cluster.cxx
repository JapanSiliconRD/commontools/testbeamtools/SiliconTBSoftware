#include "Cluster.hh"

int Cluster::GetClusterSize(int axis){
  if(axis==0){
    return hitsforCluster.size();
  }else if(axis==1){
    std::vector<int> unicol;
    for(unsigned int iclu=0;iclu<hitsforCluster.size();iclu++){
      bool isExist=false;
      for(unsigned int iucol=0;iucol<unicol.size();iucol++){
	if(unicol[iucol]==hitsforCluster[iclu].Col()){
	  isExist=true;
	  break;
	}
      }
      if(!isExist)unicol.push_back(hitsforCluster[iclu].Col());
    }
    return unicol.size();
  }else if(axis==2){
    std::vector<int> unirow;
    for(unsigned int iclu=0;iclu<hitsforCluster.size();iclu++){
      bool isExist=false;
      for(unsigned int iurow=0;iurow<unirow.size();iurow++){
	if(unirow[iurow]==hitsforCluster[iclu].Row()){
	  isExist=true;
	  break;
	}
      }
      if(!isExist)unirow.push_back(hitsforCluster[iclu].Row());
      return unirow.size();
    }
  }else{
    return -1;
  }
  return -2;
}
void Cluster::GetClusterPosition(int type){
  posX=0; posY=0;posZ=0;sumToT=0;sumToT2=0;
  for(unsigned int ihit=0;ihit<hitsforCluster.size();ihit++){
    if(type==0){
      posX+=hitsforCluster[ihit].PosX();
      posY+=hitsforCluster[ihit].PosY();
      posZ+=hitsforCluster[ihit].PosZ();
    }else if(type==1){
      double tot=hitsforCluster[ihit].ToT()-1;
      posX+=tot*hitsforCluster[ihit].PosX();
      posY+=tot*hitsforCluster[ihit].PosY();
      posZ+=tot*hitsforCluster[ihit].PosZ();
    }else {
      std::cout << "type " << type << " not defined" << std::endl;
    }
    sumToT+=hitsforCluster[ihit].ToT()-1;
    sumToT2+=hitsforCluster[ihit].ToT2()-1;
  }
  if(type==0){
    posX/=hitsforCluster.size();
    posY/=hitsforCluster.size();
    posZ/=hitsforCluster.size();
  }else if(type==1){
    posX/=sumToT;
    posY/=sumToT;
    posZ/=sumToT;
  }else {
    std::cout << "type " << type << " not defined" << std::endl;
  }
  if(hitsforCluster.size()>0){
    Module mod=hitsforCluster[0].GetModuleInfo();
    posXe=mod.colsize/sqrt(12);
    posYe=mod.rowsize/sqrt(12);
  }
}
