# Software package for Silicon R&D
##  Author Koji Nakamura <Koji.Nakamura@cern.ch>
####   first created on 11th Jan 2017


## [INSTALL]
### before install this software :
####  setup g++ >=4.8
####  setup ROOT (root 6 is recommended)
####  example at lxplus or lxatut
    git clone https://:@gitlab.cern.ch:8443/kojin/SiliconTBSoftware.git SiliconTBSoftware-master
    cd SiliconTBSoftware-master/config
    source setupATLAS.sh
    ./configure.sh
    source setup_local.sh

####  configure.sh need only once when you install the package. next time you login :
    source setup_local.sh


## [COMPILE]
### check config/ListOfPackages.txt to select packages to be included.

    cd $SISOFTDIR
    make

## [for OnlineMonitor]

    cd $SISOFTDIR/OnlineMonitor/src
    make

## [EXECUTE Online Monitor]

    cd $SISOFTDIR/OnlineMonitor/src
    ../bin/onlinemon config.txt

## [for OfflineAnalysis]

    cd $SISOFTDIR/OfflineAnalysis/src
    make

## [EXECUTE Analysis]

    cd $SISOFTDIR/OfflineAnalysis/src
    ../bin/analysis config.txt



